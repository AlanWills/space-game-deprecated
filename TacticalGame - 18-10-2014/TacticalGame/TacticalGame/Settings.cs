﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TacticalGame
{
    public static class Settings
    {
        public static float MusicVolume;
        public static float SoundVolume;
        public static int ScreenResolutionX;
        public static int ScreenResolutionY;
    }
}
