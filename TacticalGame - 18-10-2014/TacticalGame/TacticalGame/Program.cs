using System;

namespace TacticalGame
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (TacticalGame game = new TacticalGame())
            {
                game.Run();
            }
        }
    }
#endif
}

