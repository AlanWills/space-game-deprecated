﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TacticalGame
{
    public class CameraManager
    {
         #region Fields

        protected float _zoom;
        protected Matrix _transform;
        protected Matrix _inverseTransform;
        protected Vector2 _pos;
        protected float _rotation;
        protected Viewport _viewport;
        protected MouseState _mState;
        protected KeyboardState _keyState;
        protected Int32 _scroll;
        protected bool _isActive;

        #endregion

        #region Properties

        public float Zoom
        {
            get { return _zoom; }
            set { _zoom = value; }
        }
        /// <summary>
        /// Camera View Matrix Property
        /// </summary>
        public Matrix Transform
        {
            get { return _transform; }
            set { _transform = value; }
        }
        /// <summary>
        /// Inverse of the view matrix, can be used to get objects screen coordinates
        /// from its object coordinates
        /// </summary>
        public Matrix InverseTransform
        {
            get { return _inverseTransform; }
        }
        public Vector2 Pos
        {
            get { return _pos; }
            set { _pos = value; }
        }
        public float Rotation
        {
            get { return _rotation; }
            set { _rotation = value; }
        }

        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        #endregion

        #region Constructor

        public CameraManager()
        {
            _zoom = 1.0f;
            _scroll = 1;
            _rotation = MathHelper.ToRadians(180);
            _pos = Vector2.Zero;
            _transform = Matrix.CreateRotationZ(_rotation) *
                                Matrix.CreateScale(new Vector3(_zoom, _zoom, 1)) *
                                Matrix.CreateTranslation(_pos.X, _pos.Y, 0);
            _isActive = false;
        }

        public void Initialize(ScreenSystem ss)
        {
            _viewport = ss.Viewport;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Update the camera view
        /// </summary>
        public void Update()
        {
            //Clamp zoom value
            if (_isActive)
            {
                // Pos = ClampCameraPosition(_pos);
                _zoom = MathHelper.Clamp(_zoom, 0.5f, 5.0f);
                //Clamp rotation value
                _rotation = ClampAngle(_rotation);
                //Create view matrix
                _transform = Matrix.CreateRotationZ(_rotation) *
                                Matrix.CreateScale(new Vector3(_zoom, _zoom, 1)) *
                                Matrix.CreateTranslation(_pos.X, _pos.Y, 0);
                //Update inverse matrix
                _inverseTransform = Matrix.Invert(_transform);
            }
        }

        /// <summary>
        /// Clamps a radian value between -pi and pi
        /// </summary>
        /// <param name="radians">angle to be clamped</param>
        /// <returns>clamped angle</returns>
        protected float ClampAngle(float radians)
        {
            while (radians < -MathHelper.Pi)
            {
                radians += MathHelper.TwoPi;
            }
            while (radians > MathHelper.Pi)
            {
                radians -= MathHelper.TwoPi;
            }
            return radians;
        }

        private Vector2 ClampCameraPosition(Vector2 p)
        {
            if (p.X > 0) p.X = 0;
            if (p.Y > 0) p.Y = 0;
            if (p.X < -1040) p.X = -1040;
            if (p.Y < -976) p.Y = -976;

            return p;
        }

        #endregion
    }
}
