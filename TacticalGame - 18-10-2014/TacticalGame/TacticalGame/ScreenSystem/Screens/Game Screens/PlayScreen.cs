﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Engine;

namespace TacticalGame
{
    public class PlayScreen : GameScreen
    {
        UserInterface ui;
        Map map;
        Session session;
        List<Turret> towers;
        SpriteFont font;
        LevelSelectionScreen levelSelect;

        public override bool AcceptsInput
        {
            get { return true; }
        }

        public PlayScreen(LevelSelectionScreen lss, Map m)
        {
            map = m;
            levelSelect = lss;

            towers = new List<Turret>(10);
            session = new Session(m);

            session.MapFinished += new EventHandler(session_MapFinished);
        }

        void session_MapFinished(object sender, EventArgs e)
        {
            ExitScreen();
        }

        public override void InitializeScreen()
        {
            // Change for dynamic resolutions
            ui = new UserInterface(
                new MapRegion(session, new Rectangle(0, 0, 960, 690), ScreenSystem.GraphicsDevice),
                new WaveInformation(session, new Rectangle(0, 994, 1280, 30), ScreenSystem.GraphicsDevice, Color.Black),
                new CommandInfoBar(session, new Rectangle(960, 0, 320, 1024), ScreenSystem.GraphicsDevice),
                session);

            AudioManager.singleton.PlaySong(map.SongCueName);
            InputMap.NewAction("Pause", Microsoft.Xna.Framework.Input.Keys.Escape);
            InputMap.NewAction("MoveLeft", Microsoft.Xna.Framework.Input.Keys.A);
            InputMap.NewAction("MoveRight", Microsoft.Xna.Framework.Input.Keys.D);
            InputMap.NewAction("MoveUp", Microsoft.Xna.Framework.Input.Keys.W);
            InputMap.NewAction("MoveDown", Microsoft.Xna.Framework.Input.Keys.S);
            InputMap.NewAction("ZoomIn", Microsoft.Xna.Framework.Input.Keys.E);
            InputMap.NewAction("ZoomOut", Microsoft.Xna.Framework.Input.Keys.Q);

            ScreenSystem.Camera.IsActive = true;
        }

        public override void LoadContent()
        {
            ui.Font = ScreenSystem.Content.Load<SpriteFont>("Fonts\\playfont");

            Ally playerShip = ScreenSystem.Content.Load<Ally>(String.Format("Allies\\StartingShip"));
            playerShip.Position = new Vector2(0, 500);
            session.ActivePlayer.PlayerShips.Add(playerShip);
        }

        protected override void UpdateScreen(Microsoft.Xna.Framework.GameTime gameTime)
        {
            if (session.IsPaused)
            {
                FreezeScreen();
                ScreenSystem.Camera.IsActive = false;
                ScreenSystem.AddScreen(new PauseScreen(this, session));
            }

            if (InputMap.NewActionPress("Pause"))
            {
                session.Pause();
            }

            UpdateCamera();
            session.Update(gameTime);
        }

        private void UpdateCamera()
        {
            if (InputMap.ActionPressed("MoveLeft"))
            {
                ScreenSystem.Camera.Pos += new Vector2(4, 0);
            }
            if (InputMap.ActionPressed("MoveRight"))
            {
                ScreenSystem.Camera.Pos += new Vector2(-4, 0);
            }
            if (InputMap.ActionPressed("MoveUp"))
            {
                ScreenSystem.Camera.Pos += new Vector2(0, 4);
            }
            if (InputMap.ActionPressed("MoveDown"))
            {
                ScreenSystem.Camera.Pos += new Vector2(0, -4);
            }
            if (InputMap.ActionPressed("ZoomIn"))
            {
                ScreenSystem.Camera.Zoom += 0.05f;
            }
            if (InputMap.ActionPressed("ZoomOut"))
            {
                ScreenSystem.Camera.Zoom -= 0.05f;
            }
        }

        protected override void DrawScreen(Microsoft.Xna.Framework.GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenSystem.SpriteBatch;

            session.Draw(gameTime, spriteBatch);
        }

        protected override void DrawCameraIndependentElements(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenSystem.SpriteBatch;

            ui.DrawUI(gameTime, spriteBatch);
        }
    }
}
