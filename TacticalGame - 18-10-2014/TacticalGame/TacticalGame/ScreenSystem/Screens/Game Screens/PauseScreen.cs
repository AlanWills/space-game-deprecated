﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Engine;

namespace TacticalGame
{
    public class PauseScreen : MenuScreen
    {
        string prevEntry, nextEntry, selectedEntry, cancelMenu;

        public override string MenuCancelActionName
        {
            get { return cancelMenu; }
        }

        public override string NextEntryActionName
        {
            get { return nextEntry; }
        }

        public override string PreviousEntryActionName
        {
            get { return prevEntry; }
        }

        public override string SelectedEntryActionName
        {
            get { return selectedEntry; }
        }

        MainMenuEntry resume, options, help, quit;
        GameScreen screenBefore;
        Session session;

        public PauseScreen(GameScreen before, Session s)
        {
            prevEntry = "MenuUp";
            nextEntry = "MenuDown";
            selectedEntry = "MenuAccept";
            cancelMenu = "MenuCancel";

            TransitionOnTime = TimeSpan.FromSeconds(1);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);

            Selected = Highlighted = new Color(214, 232, 223);
            Normal = new Color(104, 173, 178);

            screenBefore = before;
            session = s;
        }

        public override void InitializeScreen()
        {
            InputMap.NewAction(PreviousEntryActionName, Keys.Up);
            InputMap.NewAction(NextEntryActionName, Keys.Down);
            InputMap.NewAction(SelectedEntryActionName, Keys.Enter);
            InputMap.NewAction(SelectedEntryActionName, MousePresses.LeftMouse);
            InputMap.NewAction(MenuCancelActionName, Keys.Escape);

            resume = new MainMenuEntry(this, "Resume", "CONTINUE PLAYING THE GAME");
            options = new MainMenuEntry(this, "Options", "CHANGE GAME SETTINGS");
            help = new MainMenuEntry(this, "Help", "INPUT DIAGRAM AND GENERAL GAME INFORMATION");
            quit = new MainMenuEntry(this, "Quit", "DONE PLAYING FOR NOW?");

            resume.Selected += new EventHandler(ResumeSelect);
            options.Selected += new EventHandler(OptionsSelect);
            help.Selected += new EventHandler(HelpSelect);
            quit.Selected += new EventHandler(QuitSelect);

            Removing += new EventHandler(PauseScreenRemoving);
            Entering += new TransitionEventHandler(MainMenuScreen_Entering);
            Exiting += new TransitionEventHandler(MainMenuScreen_Exiting);

            MenuEntries.Add(resume);
            MenuEntries.Add(options);
            MenuEntries.Add(help);
            MenuEntries.Add(quit);

            Viewport view = ScreenSystem.Viewport;
            SetDescriptionArea(
                new Rectangle(100, view.Height - 100, view.Width - 100, 50),
                new Color(11, 38, 40),
                new Color(29, 108, 117),
                new Point(10, 0),
                0.5f);

            AudioManager.singleton.PlaySong("Menu");
        }

        public override void LoadContent()
        {
            ContentManager content = ScreenSystem.Content;
            SpriteFont = content.Load<SpriteFont>(@"Fonts/menu");

            Texture2D entryTexture = content.Load<Texture2D>(@"Textures/Menu/MenuEntries");

            BackgroundTexture = content.Load<Texture2D>(@"Textures/Menu/MainMenuBackground");
            TitleTexture = content.Load<Texture2D>(@"Textures/Menu/LogoWithText");

            EnableMouse(content.Load<Texture2D>(@"Textures/Menu/mouse"));

            InitialTitlePosition = TitlePosition =
                new Vector2((ScreenSystem.Viewport.Width - TitleTexture.Width) / 2, 50);

            for (int i = 0; i < MenuEntries.Count; i++)
            {
                MenuEntries[i].AddTexture(entryTexture, 2, 1,
                    new Rectangle(0, 0, entryTexture.Width / 2, entryTexture.Height),
                    new Rectangle(entryTexture.Width / 2, 0, entryTexture.Width / 2, entryTexture.Height));
                MenuEntries[i].AddPadding(14, 0);

                if (i == 0)
                    MenuEntries[i].SetPosition(new Vector2(180, 250), true);
                else
                {
                    int offsetY = MenuEntries[i - 1].EntryTexture == null ? SpriteFont.LineSpacing
                        : 8;
                    MenuEntries[i].SetRelativePosition(new Vector2(0, offsetY), MenuEntries[i - 1], true);
                }
            }
        }

        public override void UnloadContent()
        {
            SpriteFont = null;
        }

        void MainMenuScreen_Exiting(object sender, TransitionEventArgs tea)
        {
            float effect = (float)Math.Pow(tea.percent - 1, 2) * -100;
            foreach (MenuEntry entry in MenuEntries)
            {
                entry.Acceleration = new Vector2(effect, 0);
                entry.Position = entry.InitialPosition + entry.Acceleration;
                entry.Scale = tea.percent;
                entry.Opacity = tea.percent;
            }

            TitlePosition = InitialTitlePosition - new Vector2(0, effect);
            TitleOpacity = tea.percent;
        }

        void MainMenuScreen_Entering(object sender, TransitionEventArgs tea)
        {
            float effect = (float)Math.Pow(tea.percent - 1, 2) * 100;
            foreach (MenuEntry entry in MenuEntries)
            {
                entry.Acceleration = new Vector2(effect, 0);
                entry.Position = entry.InitialPosition + entry.Acceleration;
                entry.Opacity = tea.percent;
            }

            // Should this be a minus?
            TitlePosition = InitialTitlePosition - new Vector2(0, effect);
            TitleOpacity = tea.percent;
        }

        void ResumeSelect(object sender, EventArgs e)
        {
            ExitScreen();
        }

        void OptionsSelect(object sender, EventArgs e)
        {
            FreezeScreen();
            ScreenSystem.AddScreen(new OptionsScreen(this));
        }

        void HelpSelect(object sender, EventArgs e)
        {
            FreezeScreen();
            ScreenSystem.AddScreen(new HelpScreen(this));
        }

        void QuitSelect(object sender, EventArgs e)
        {
            ScreenSystem.Game.Exit();
        }

        void PauseScreenRemoving(object sender, EventArgs e)
        {
            MenuEntries.Clear();
            screenBefore.ActivateScreen();
            session.Resume();
        }
    }
}
