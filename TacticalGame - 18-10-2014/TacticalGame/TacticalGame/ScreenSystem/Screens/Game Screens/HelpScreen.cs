﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using TacticalGame;

namespace TacticalGame
{
    public class HelpScreen : GameScreen
    {
        SpriteFont font;
        GameScreen screenBefore;

        List<Texture2D> helpTextures;
        int helpTexturesCount;

        int index;

        public override bool AcceptsInput
        {
            get { return true; }
        }

        public HelpScreen(GameScreen before)
        {
            screenBefore = before;
        }

        public override void InitializeScreen()
        {
            InputMap.NewAction("Finished", Keys.Escape);
            InputMap.NewAction("Next", Keys.Space);

            helpTexturesCount = 3;
            index = 0;
            helpTextures = new List<Texture2D>(helpTexturesCount);

            EnableFade(Color.Black, 0.85f);
        }

        public override void LoadContent()
        {
            ContentManager content = ScreenSystem.Content;
            font = content.Load<SpriteFont>(@"Fonts/help");

            for (int i = 0; i < helpTexturesCount; i++)
            {
                helpTextures.Add(content.Load<Texture2D>(
                    string.Format("Textures\\Help\\help_{0}", i + 1)));
            }
        }

        protected override void UpdateScreen(Microsoft.Xna.Framework.GameTime gameTime)
        {
            if (InputMap.NewActionPress("Finished"))
            {
                ExitScreen();
                screenBefore.ActivateScreen();
            }

            if (InputMap.NewActionPress("Next"))
            {
                if (index >= (helpTexturesCount - 1))
                {
                    index = 0;
                }
                else
                {
                    index++;
                }
            }
        }

        protected override void DrawScreen(Microsoft.Xna.Framework.GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenSystem.SpriteBatch;

            spriteBatch.Draw(helpTextures[index],
                new Rectangle(0, 0, 1280, 720),
                Color.White);

            spriteBatch.DrawString(font, "Press Space to advance to the next image",
                Vector2.Zero, Color.Red);
        }
    }
}
