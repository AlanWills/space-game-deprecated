﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using TacticalGame;

namespace TacticalGame
{
    public class GameOverScreen : MenuScreen
    {
        MainMenuEntry play, quit;

        string prevEntry, nextEntry, selectedEntry, cancelMenu;
        public override string MenuCancelActionName
        {
            get { return cancelMenu; }
        }

        public override string NextEntryActionName
        {
            get { return nextEntry; }
        }

        public override string PreviousEntryActionName
        {
            get { return prevEntry; }
        }

        public override string SelectedEntryActionName
        {
            get { return selectedEntry; }
        }

        LevelSelectionScreen levelSelect;

        public GameOverScreen(LevelSelectionScreen lss)
        {
            prevEntry = "MenuUp";
            nextEntry = "MenuDown";
            selectedEntry = "MenuAccept";
            cancelMenu = "MenuCancel";

            TransitionOnTime = TimeSpan.FromSeconds(1);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);

            Selected = Highlighted = new Microsoft.Xna.Framework.Color(214, 232, 223);
            Normal = new Microsoft.Xna.Framework.Color(104, 173, 178);

            levelSelect = lss;
        }

        public override void InitializeScreen()
        {
            InputMap.NewAction(PreviousEntryActionName, Keys.Up);
            InputMap.NewAction(NextEntryActionName, Keys.Down);
            InputMap.NewAction(SelectedEntryActionName, Keys.Enter);
            InputMap.NewAction(MenuCancelActionName, Keys.Escape);

            play = new MainMenuEntry(this, "Play Again?", "GAME OVER - PLAY THE GAME AGAIN?");
            quit = new MainMenuEntry(this, "Quit?", "DONE PLAYING FOR NOW?");

            Removing += new EventHandler(GameOverScreen_Removing);
            Entering += new TransitionEventHandler(GameOverScreen_Entering);
            Exiting += new TransitionEventHandler(GameOverScreen_Exiting);

            play.Selected += new EventHandler(play_Selected);
            quit.Selected += new EventHandler(quit_Selected);

            MenuEntries.Add(play);
            MenuEntries.Add(quit);

            Viewport view = ScreenSystem.Viewport;
            SetDescriptionArea(new Rectangle(100, view.Height - 100, view.Width - 100, 50),
                new Color(11, 38, 40), new Color(29, 108, 117), new Point(10, 0), 0.5f);
        }

        public override void LoadContent()
        {
            ContentManager content = ScreenSystem.Content;
            SpriteFont = content.Load<SpriteFont>(@"Fonts/menu");

            Texture2D entryTexture = content.Load<Texture2D>(@"Textures/Menu/MenuEntries");
            BackgroundTexture = content.Load<Texture2D>(@"Textures/Menu/MainMenuBackground");
            TitleTexture = content.Load<Texture2D>(@"Textures/Menu/LogoWithText");

            // Change InitialTitlePosition to be different to have animation on title
            InitialTitlePosition = TitlePosition = new Vector2((ScreenSystem.Viewport.Width - TitleTexture.Width) / 2, 50);

            for (int i = 0; i < MenuEntries.Count; i++)
            {
                MenuEntries[i].AddTexture(entryTexture, 2, 1,
                    new Rectangle(0, 0, entryTexture.Width / 2, entryTexture.Height),
                    new Rectangle(entryTexture.Width / 2, 0, entryTexture.Width / 2, entryTexture.Height));

                MenuEntries[i].AddPadding(14, 0);

                if (i == 0)
                {
                    MenuEntries[i].SetPosition(new Vector2(180, 250), true);
                }
                else
                {
                    int offsetY = MenuEntries[i - 1].EntryTexture == null ? SpriteFont.LineSpacing : 8;
                    MenuEntries[i].SetRelativePosition(new Vector2(0, offsetY), MenuEntries[i - 1], true);
                }
            }
        }

        public override void UnloadContent()
        {
            SpriteFont = null;
        }

        void GameOverScreen_Removing(object sender, EventArgs tea)
        {
            MenuEntries.Clear();
        }

        void GameOverScreen_Entering(object sender, TransitionEventArgs tea)
        {
            float effect = (float)Math.Pow(tea.percent - 1, 2) * -100;

            foreach (MenuEntry entry in MenuEntries)
            {
                entry.Acceleration = new Vector2(effect, 0);
                entry.Position = entry.InitialPosition + entry.Acceleration;
                entry.Scale = tea.percent;
                entry.Opacity = tea.percent;
            }

            TitlePosition = InitialTitlePosition - new Vector2(0, effect);
            TitleOpacity = tea.percent;
        }

        void GameOverScreen_Exiting(object sender, TransitionEventArgs tea)
        {
            float effect = (float)Math.Pow(tea.percent - 1, 2) * 100;

            foreach (MenuEntry entry in MenuEntries)
            {
                entry.Acceleration = new Vector2(effect, 0);
                entry.Position = entry.InitialPosition + entry.Acceleration;
                entry.Scale = tea.percent;
                entry.Opacity = tea.percent;
            }

            TitlePosition = InitialTitlePosition - new Vector2(0, effect);
            TitleOpacity = tea.percent;
        }

        void quit_Selected(object sender, EventArgs e)
        {
            ScreenSystem.Game.Exit();
        }

        void play_Selected(object sender, EventArgs e)
        {
            ExitScreen();
            levelSelect.ActivateScreen();
        }
    }
}
