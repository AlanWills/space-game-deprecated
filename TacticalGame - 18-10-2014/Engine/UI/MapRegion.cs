﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Engine
{
    // This refers to the whole playable map area
    public class MapRegion
    {
        #region Properties and Fields

        public Rectangle Rectangle
        {
            get;
            private set;
        }

        public Session Session
        {
            get;
            private set;
        }

        public Texture2D EnemyHealthDisplay
        {
            get;
            private set;
        }

        public Ship SelectedActiveShip
        {
            get;
            private set;
        }

        public Turret SelectedActiveTurret
        {
            get;
            private set;
        }

        #endregion

        #region Constructor

        public MapRegion(Session s, Rectangle r, GraphicsDevice gd)
        {
            Session = s;
            Rectangle = r;

            EnemyHealthDisplay = new Texture2D(gd, 1, 1);
            Color[] c = new Color[1];
            c[0] = Session.Map.ForeColour;
            EnemyHealthDisplay.SetData<Color>(c);

            Session.ShipPurchased += new Session.PurchaseShipEventHandler(Session_ShipPurchased);
            Session.ShipSold += new Session.SellShipEventHandler(Session_ShipSold);
            Session.TurretPurchased += new Session.PurchaseTurretEventHandler(Session_TurretPurchased);
            Session.TurretSold += new Session.SellTurretEventHandler(Session_TurretSold);
        }

        #endregion

        #region Methods

        void Session_ShipSold(object sender, ShipEventArgs ssea)
        {
            if (ssea.s == SelectedActiveShip) SelectedActiveShip = null;
        }

        void Session_ShipPurchased(object sender, ShipEventArgs psea)
        {
            psea.s.LeftClickEvent += new EventHandler(s_LeftClickEvent);
        }

        void Session_TurretSold(object sender, TurretEventArgs ptea)
        {
            if (ptea.t == SelectedActiveTurret) SelectedActiveTurret = null;
        }

        void Session_TurretPurchased(object sender, TurretEventArgs ptea)
        {
            ptea.t.LeftClickEvent += new EventHandler(t_LeftClickEvent);
        }

        void s_LeftClickEvent(object sender, EventArgs e)
        {
            Ship s = sender as Ship;

            SelectedActiveShip = s;
        }

        void t_LeftClickEvent(object sender, EventArgs e)
        {
            Turret t = sender as Turret;
            // Increase the rhs to have a build time
            if (t.PlacedTime > 1)
                SelectedActiveTurret = t;
        }


        public void Update(GameTime gameTime)
        {
            if (Session.UI.Mouse.NewLeftClick && Rectangle.Intersects(Session.UI.Mouse.Rectangle) && Session.SelectedShip != null)
            {
                Point mousePointerInMap = new Point((int)Session.UI.Mouse.Position.X, (int)Session.UI.Mouse.Position.Y);

                /*if (Session.Map.IsValidPlacement(mousePointerInMap.X, mousePointerInMap.Y))
                {
                    Session.PurchaseTurret(Session.SelectedTurret.Clone(), mousePointerInMap);
                    Session.DeselectTurret();
                }*/
            }
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch, SpriteFont spriteFont)
        {
            Map map = Session.Map;

            if (map.BackgroundTexture != null)
            {
                spriteBatch.Draw(map.BackgroundTexture, new Rectangle(0, 0, map.Dimensions.X, map.Dimensions.Y), Color.White);
            }

            foreach (Enemy m in Session.Map.ActiveWave.Enemies)
            {
                // m.Draw(gameTime, spriteBatch);
                // spriteBatch.Draw(EnemyHealthDisplay, new Rectangle(m.Rectangle.Left, m.Rectangle.Top - 4, (int)(m.Rectangle.Width * ((float)m.Health / (float)m.MaxHealth)), 2), Color.White);
            }

            if (SelectedActiveShip != null)
            {
                // SelectedActiveShip.DrawRadius(gameTime, spriteBatch);
            }

            /*foreach (Ship s in Session.ActivePlayer.PlayerShips)
            {
                s.Draw(gameTime, spriteBatch);
            }*/
        }

        internal void ResetTurretReferences()
        {
            SelectedActiveTurret = null;
        }

        #endregion
    }
}
