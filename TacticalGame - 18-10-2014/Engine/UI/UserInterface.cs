﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Engine
{
    public class UserInterface
    {
        #region Properties and Fields

        public MapRegion MapRegion
        {
            get;
            private set;
        }

        public WaveInformation WaveInformation
        {
            get;
            private set;
        }

        public CommandInfoBar CommandInfoBar
        {
            get;
            private set;
        }

        SpriteFont font;
        public SpriteFont Font
        {
            get { return font; }
            set
            {
                font = value;
                CommandInfoBar.Initialize(value);
            }
        }

        Session session;

        public Mouse Mouse
        {
            get;
            private set;
        }

        #endregion

        #region Contructor

        public UserInterface(MapRegion mapRegion, WaveInformation waveInfo, CommandInfoBar commandInfoBar, Session s)
        {
            MapRegion = mapRegion;
            WaveInformation = waveInfo;
            CommandInfoBar = commandInfoBar;
            session = s;
            Mouse = new Mouse(session.Map.MouseTexture);
            session.SetUI(this);
        }

        #endregion

        #region Methods

        public void UpdateUI(GameTime gameTime)
        {
            Mouse.Update();
            MapRegion.Update(gameTime);
            WaveInformation.Update(gameTime);
            CommandInfoBar.Update(gameTime);
        }

        public void DrawUI(GameTime gameTime, SpriteBatch spriteBatch)
        {
            MapRegion.Draw(gameTime, spriteBatch, font);
            WaveInformation.Draw(gameTime, spriteBatch, font);
            CommandInfoBar.Draw(gameTime, spriteBatch, font);

            Mouse.Draw(spriteBatch);
        }

        #endregion
    }
}
