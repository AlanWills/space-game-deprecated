﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Engine
{
    public class CommandInfoBar
    {
        #region Properties and Fields

        public Rectangle Rectangle
        {
            get;
            private set;
        }

        public UIBlock PurchaseShip
        {
            get;
            private set;
        }

        public UIBlock SelectedShip
        {
            get;
            private set;
        }

        public UIBlock MoneyAndTurrets
        {
            get;
            private set;
        }

        public UIBlock StatsAndControls
        {
            get;
            private set;
        }

        public Session Session
        {
            get;
            private set;
        }

        int padding, waveindex;

        Texture2D background;

        SpriteFont spriteFont;

        Ship clickedShip;

        #endregion

        #region Constructor

        public CommandInfoBar(Session s, Rectangle r, GraphicsDevice gd)
        {
            Session = s;
            Session.ShipPurchased += new Session.PurchaseShipEventHandler(Session_ShipPurchased);
            // Session.TurretPurchased += new Session.PurchaseTurretEventHandler(Session_TurretPurchased);
            Session.MoneyIncreased += new EventHandler(Session_MoneyIncreased);
            Rectangle = r;
            padding = 10;
            waveindex = Session.Map.WaveIndex;

            MoneyAndTurrets = new UIBlock(gd, null, s.Map.BorderColour,
                new Rectangle(r.X, r.Y, r.Width - 5, 50), s);
            PurchaseShip = new UIBlock(gd, s.Map.BorderTexture, s.Map.BorderColour,
                new Rectangle(r.X, MoneyAndTurrets.Dimensions.Bottom + 10, r.Width - 5, 420), s);
            SelectedShip = new UIBlock(gd, s.Map.BorderTexture, s.Map.BorderColour,
                new Rectangle(r.X, MoneyAndTurrets.Dimensions.Bottom + 10, r.Width - 5, 420), s);
            StatsAndControls = new UIBlock(gd, s.Map.BorderTexture, s.Map.BorderColour,
                new Rectangle(r.X, PurchaseShip.Dimensions.Bottom + 10, r.Width - 5, 200), s);
        }

        #endregion

        #region Methods

        void Session_MoneyIncreased(object sender, EventArgs e)
        {
            /*Button bt = SelectedTurret.GetButton("BuyTurret");
            Button ut = SelectedTurret.GetButton("UpgradeTurret");

            if (bt != null)
            {
                if (clickedTurret != null && clickedTurret.Cost <= Session.ActivePlayer.Money)
                {
                    bt.Texture = Session.Map.SmallNormalButtonTexture;
                    bt.SetColour(Session.Map.ForeColour);

                    if (bt.State == UIButtonState.Inactive)
                    {
                        bt.LeftClickEvent += new EventHandler(buyTurret_LeftClick);
                        bt.Activate();
                    }
                }
            }

            else if (ut != null)
            {
                if (clickedTurret != null && clickedTurret.UpgradeCost <= Session.ActivePlayer.Money
                    && clickedTurret.Level + 1 < clickedTurret.MaxLevel)
                {
                    ut.Texture = Session.Map.SmallNormalButtonTexture;
                    ut.SetColour(Session.Map.ForeColour);

                    if (ut.State == UIButtonState.Inactive)
                    {
                        ut.LeftClickEvent += new EventHandler(upgradeTurret_LeftClick);
                        ut.Activate();
                    }
                }
            }*/
        }

        void Session_ShipPurchased(object sender, ShipEventArgs psea)
        {
            psea.s.LeftClickEvent += new EventHandler(clickableShip_LeftClickEvent);
            Button b = SelectedShip.GetButton("BuyShip");

            Ally a = clickedShip as Ally;

            if (a != null)
            {
                if (a.Cost > Session.ActivePlayer.Money)
                {
                    b.Texture = Session.Map.SmallErrorButtonTexture;
                    b.SetColour(Session.Map.ErrorColour);

                    if (b.State == UIButtonState.Active)
                    {
                        b.LeftClickEvent -= buyTurret_LeftClick;
                        b.Deactivate();
                    }
                }
            }
        }

        void Session_TurretPurchased(object sender, TurretEventArgs ptea)
        {
            /* ptea.t.LeftClickEvent += new EventHandler(clickableTurret_LeftClickEvent);
            Button b = SelectedTurret.GetButton("BuyTurret");
            if (clickedTurret.Cost > Session.ActivePlayer.Money)
            {
                b.Texture = Session.Map.SmallErrorButtonTexture;
                b.SetColour(Session.Map.ErrorColour);

                if (b.State == UIButtonState.Active)
                {
                    b.LeftClickEvent -= buyTurret_LeftClick;
                    b.Deactivate();
                }
            }*/
        }

        public void Initialize(SpriteFont spriteFont)
        {
            this.spriteFont = spriteFont;
            InitializeMoneyAndTurrets();
            InitializePurchaseShip();
            InitializeStatsAndControls();
        }

        void clickableShip_LeftClickEvent(object sender, EventArgs e)
        {
            Ship s = sender as Ship;

            clickedShip = s;
            InitializeSelectedShip(s);
        }

        private void InitializeSelectedShip(Ship s)
        {
            SelectedShip.ClearAll();

            Image icon = new Image(s.Texture, new Vector2(SelectedShip.Dimensions.Left,
                SelectedShip.Dimensions.Top + padding));
            SelectedShip.Add("TurretIcon", icon);

            SelectedShip.Add("TurretName", new Text(s.Name + " " +
                (s.Level + 1).ToString(), spriteFont,
                new Vector2(icon.Rectangle.Right + padding, SelectedShip.Dimensions.Top + padding)));
            SelectedShip.Add("Description", new Text(s.Description, spriteFont,
                new Vector2(icon.Rectangle.Right + padding, SelectedShip.Dimensions.Top + padding + spriteFont.LineSpacing)));

            Text stats = new Text(s.CurrentStatistics.ToShortString(), spriteFont,
                new Vector2(SelectedShip.Dimensions.Left + padding, icon.Rectangle.Bottom));
            SelectedShip.Add("Stats", stats);

            Ally a = s as Ally;

            if (a != null)
            {
                Text price = new Text(String.Format("Price: {0}", a.TotalCost), spriteFont, new Vector2(SelectedShip.Dimensions.Left + padding, SelectedShip.Dimensions.Bottom));
                SelectedShip.Add("Price", price);
            }

            /*string str = s.IsActive ? "Deselect Turret" : "Cancel";
            Vector2 sdim = spriteFont.MeasureString(str);

            Vector2 cbpos = new Vector2((int)(SelectedShip.Dimensions.Left + (Session.Map.SmallNormalButtonTexture.Width / 2.0f) +
                (SelectedShip.Dimensions.Width - Session.Map.SmallNormalButtonTexture.Width) / 2.0f), (int)(SelectedShip.Dimensions.Bottom - (Session.Map.SmallNormalButtonTexture.Height / 2.0f) - padding));

            Vector2 ctpos = new Vector2((int)(cbpos.X - Session.Map.SmallNormalButtonTexture.Width / 2.0f + padding),
                (int)(SelectedShip.Dimensions.Bottom - (Session.Map.SmallNormalButtonTexture.Height + sdim.Y) / 2.0f - padding));

            Button cb = new Button(Session.Map.SmallNormalButtonTexture, cbpos, new Text(s, spriteFont, ctpos), Session.Map.ForeColour, null);
            cb.LeftClickEvent += new EventHandler(cancelButton_LeftClick);
            SelectedShip.Add("Cancel", cb);*/
        }

        void selectShip_LeftClick(object sender, EventArgs e)
        {
            if (clickedShip == null)
            {
                Button b = sender as Button;
                if (b != null)
                {
                    Ship s = b.StoredObject as Ship;
                    if (s != null)
                    {
                        clickedShip = s;
                        InitializeSelectedShip(s);
                    }
                }
            }
        }

        private void InitializeMoneyAndTurrets()
        {
            MoneyAndTurrets.Add("Money", new Text(Session.MoneyDisplay,
                new Vector2(Rectangle.Left + padding, Rectangle.Top + padding)));
            /*MoneyAndTurrets.Add("Turrets", new Text(Session.TurretsDisplay,
                new Vector2(Rectangle.Left + padding, Rectangle.Top + padding + spriteFont.LineSpacing)));*/
        }

        private void InitializePurchaseShip()
        {
            PurchaseShip.Add("Purchase", new Text("Purchase a Ship", new Vector2(PurchaseShip.Dimensions.Left + padding,
                PurchaseShip.Dimensions.Top + padding)));

            Vector2 pos = new Vector2(PurchaseShip.Dimensions.Left + padding, PurchaseShip.Dimensions.Top
                + padding + (spriteFont.LineSpacing * 2));

            /*foreach (Turret t in Session.Map.TurretList)
            {
                Button b = new Button(t.Thumbnail, Vector2.Add(pos, new Vector2(t.Thumbnail.Width / 2.0f,
                    t.Thumbnail.Height / 2.0f)), t);
                b.LeftClickEvent += new EventHandler(selectTurret_LeftClick);
                PurchaseTurret.Add(t.Name, b);
                pos.X += t.Thumbnail.Width + padding;

                if (pos.X + t.Thumbnail.Width >= PurchaseTurret.Dimensions.Right)
                {
                    pos = new Vector2(PurchaseTurret.Dimensions.Left + padding,
                        pos.Y + t.Thumbnail.Height + padding);
                }
            }*/
        }

        private void InitializePurchaseTurret()
        {
            /*PurchaseTurret.Add("Purchase", new Text("Purchase a Turret", new Vector2(PurchaseTurret.Dimensions.Left + padding,
                PurchaseTurret.Dimensions.Top + padding)));

            Vector2 pos = new Vector2(PurchaseTurret.Dimensions.Left + padding, PurchaseTurret.Dimensions.Top
                + padding + (spriteFont.LineSpacing * 2));

            foreach (Turret t in Session.Map.TurretList)
            {
                Button b = new Button(t.Thumbnail, Vector2.Add(pos, new Vector2(t.Thumbnail.Width / 2.0f,
                    t.Thumbnail.Height / 2.0f)), t);
                b.LeftClickEvent += new EventHandler(selectTurret_LeftClick);
                PurchaseTurret.Add(t.Name, b);
                pos.X += t.Thumbnail.Width + padding;

                if (pos.X + t.Thumbnail.Width >= PurchaseTurret.Dimensions.Right)
                {
                    pos = new Vector2(PurchaseTurret.Dimensions.Left + padding,
                        pos.Y + t.Thumbnail.Height + padding);
                }
            }*/
        }

        void selectTurret_LeftClick(object sender, EventArgs e)
        {
            /*if (clickedTurret == null)
            {
                Button b = sender as Button;
                if (b != null)
                {
                    Turret t = b.StoredObject as Turret;
                    if (t != null)
                    {
                        clickedTurret = t;
                        InitializeSelectedTurret(t);
                    }
                }
            }*/
        }

        private void InitializeSelectedTurret(Turret t)
        {
            /*SelectedTurret.ClearAll();

            Image icon = new Image(clickedTurret.Texture, new Vector2(SelectedTurret.Dimensions.Left,
                SelectedTurret.Dimensions.Top + padding));
            SelectedTurret.Add("Turret Icon", icon);

            SelectedTurret.Add("TurretName", new Text(clickedTurret.Name + " " +
                (clickedTurret.Level + 1).ToString(), spriteFont,
                new Vector2(icon.Rectangle.Right + padding, SelectedTurret.Dimensions.Top + padding)));
            SelectedTurret.Add("TurretDescription", new Text(clickedTurret.Description, spriteFont,
                new Vector2(icon.Rectangle.Right + padding, SelectedTurret.Dimensions.Top + padding + spriteFont.LineSpacing)));

            Text stats = new Text(clickedTurret.CurrentStatistics.ToShortString(), spriteFont,
                new Vector2(SelectedTurret.Dimensions.Left + padding, icon.Rectangle.Bottom));
            SelectedTurret.Add("Stats", stats);

            Text specials = new Text(String.Format("Specials: {0}", t.BulletBase.Type == BulletType.Normal ? "None" : t.BulletBase.Type.ToString()), 
                spriteFont, new Vector2(SelectedTurret.Dimensions.Left + padding, stats.Rectangle.Bottom));
            SelectedTurret.Add("Specials", specials);

            Text price = new Text(String.Format("Price: {0}", clickedTurret.TotalCost), spriteFont, new Vector2(SelectedTurret.Dimensions.Left + padding, specials.Rectangle.Bottom));
            SelectedTurret.Add("Price", price);

            if (t.IsPlaced)
            {
                int pb = AddUpgradeButton(price.Rectangle.Bottom + padding);
                AddSellButton(pb + padding);
            }
            else
            {
                AddPurchaseButton(price.Rectangle.Bottom + padding);
            }

            string s = t.IsPlaced ? "Deselect Turret" : "Cancel";
            Vector2 sdim = spriteFont.MeasureString(s);

            Vector2 cbpos = new Vector2((int)(SelectedTurret.Dimensions.Left + (Session.Map.SmallNormalButtonTexture.Width / 2.0f) +
                (SelectedTurret.Dimensions.Width - Session.Map.SmallNormalButtonTexture.Width) / 2.0f), (int)(SelectedTurret.Dimensions.Bottom - (Session.Map.SmallNormalButtonTexture.Height / 2.0f) - padding));

            Vector2 ctpos = new Vector2((int)(cbpos.X - Session.Map.SmallNormalButtonTexture.Width / 2.0f + padding),
                (int)(SelectedTurret.Dimensions.Bottom - (Session.Map.SmallNormalButtonTexture.Height + sdim.Y) / 2.0f - padding));

            Button cb = new Button(Session.Map.SmallNormalButtonTexture, cbpos, new Text(s, spriteFont, ctpos), Session.Map.ForeColour, null);
            cb.LeftClickEvent += new EventHandler(cancelButton_LeftClick);
            SelectedTurret.Add("Cancel", cb);*/
        }

        private void AddSellButton(int y)
        {
            /*Button b = null;
            string st = String.Format("Sell Turret (Receive {0})", (int)(clickedTurret.TotalCost * clickedTurret.SellScalar));
            Vector2 stdim = spriteFont.MeasureString(st);
            Vector2 bpos = new Vector2((int)(SelectedTurret.Dimensions.Left + (Session.Map.SmallNormalButtonTexture.Width / 2.0f) +
                (SelectedTurret.Dimensions.Width - Session.Map.SmallNormalButtonTexture.Width) / 2.0f), (int)(y + (Session.Map.SmallNormalButtonTexture.Height / 2.0f)));

            Vector2 tpos = new Vector2((int)(bpos.X - Session.Map.SmallNormalButtonTexture.Width / 2.0f + padding),
                (int)(y + (Session.Map.SmallNormalButtonTexture.Height - stdim.Y) / 2.0f));

            b = new Button(Session.Map.SmallNormalButtonTexture, bpos, new Text(st, spriteFont, tpos), Session.Map.ForeColour, clickedTurret);
            b.LeftClickEvent += new EventHandler(sellTurret_LeftClick);
            SelectedTurret.Add("SellTurret", b);*/
        }

        private int AddUpgradeButton(int y)
        {
            /*Button b = null;
            if (clickedTurret.UpgradeCost <= Session.ActivePlayer.Money && clickedTurret.Level + 1 < clickedTurret.MaxLevel)
            {
                string bt = String.Format("Upgrade Turret (Costs {0})", clickedTurret.UpgradeCost);
                Vector2 btdim = spriteFont.MeasureString(bt);
                Vector2 bpos = new Vector2((int)(SelectedTurret.Dimensions.Left + (Session.Map.SmallNormalButtonTexture.Width / 2.0f) +
                    (SelectedTurret.Dimensions.Width - Session.Map.SmallNormalButtonTexture.Width) / 2.0f), (int)(y + (Session.Map.SmallNormalButtonTexture.Height / 2.0f)));

                Vector2 tpos = new Vector2((int)(bpos.X - Session.Map.SmallNormalButtonTexture.Width / 2.0f + padding),
                    (int)(y + (Session.Map.SmallNormalButtonTexture.Height - btdim.Y) / 2.0f));

                b = new Button(Session.Map.SmallNormalButtonTexture, bpos, new Text(bt, spriteFont, tpos), Session.Map.ForeColour, clickedTurret);
                b.LeftClickEvent += new EventHandler(upgradeTurret_LeftClick);
                SelectedTurret.Add("UpgradeTurret", b);
            }
            else
            {
                string bt = String.Format("Upgrade Turret (Costs {0})", clickedTurret.UpgradeCost);
                Vector2 btdim = spriteFont.MeasureString(bt);

                Vector2 bpos = new Vector2((int)(SelectedTurret.Dimensions.Left + (Session.Map.SmallErrorButtonTexture.Width / 2.0f) +
                    (SelectedTurret.Dimensions.Width - Session.Map.SmallErrorButtonTexture.Width) / 2.0f), (int)(y + (Session.Map.SmallErrorButtonTexture.Height / 2.0f)));

                Vector2 tpos = new Vector2((int)(bpos.X - Session.Map.SmallErrorButtonTexture.Width / 2.0f + padding),
                    (int)(y + (Session.Map.SmallErrorButtonTexture.Height - btdim.Y) / 2.0f));

                b = new Button(Session.Map.SmallErrorButtonTexture, bpos, new Text(bt, spriteFont, tpos), Session.Map.ErrorColour, clickedTurret);
                b.Deactivate();
                SelectedTurret.Add("UpgradeTurret", b);
            }
            return (int)(b.Position.Y - b.Origin.Y) + b.Texture.Height;*/

            return 0;
        }

        private void AddPurchaseButton(int y)
        {
            /*if (clickedTurret.Cost <= Session.ActivePlayer.Money && clickedTurret.Level < clickedTurret.MaxLevel)
            {
                string bt = String.Format("Buy Turret (Costs {0})", clickedTurret.Cost);
                Vector2 btdim = spriteFont.MeasureString(bt);
                Vector2 bpos = new Vector2((int)(SelectedTurret.Dimensions.Left + (Session.Map.SmallNormalButtonTexture.Width / 2.0f) +
                    (SelectedTurret.Dimensions.Width - Session.Map.SmallNormalButtonTexture.Width) / 2.0f), (int)(y + (Session.Map.SmallNormalButtonTexture.Height / 2.0f)));

                Vector2 tpos = new Vector2((int)(bpos.X - Session.Map.SmallNormalButtonTexture.Width / 2.0f + padding),
                    (int)(y + (Session.Map.SmallNormalButtonTexture.Height - btdim.Y) / 2.0f));

                Button b = new Button(Session.Map.SmallNormalButtonTexture, bpos, new Text(bt, spriteFont, tpos), Session.Map.ForeColour, clickedTurret);
                b.LeftClickEvent += new EventHandler(buyTurret_LeftClick);
                SelectedTurret.Add("BuyTurret", b);
            }
            else
            {
                string bt = String.Format("Buy Turret (Costs {0})", clickedTurret.Cost);
                Vector2 btdim = spriteFont.MeasureString(bt);

                Vector2 bpos = new Vector2((int)(SelectedTurret.Dimensions.Left + (Session.Map.SmallErrorButtonTexture.Width / 2.0f) +
                    (SelectedTurret.Dimensions.Width - Session.Map.SmallErrorButtonTexture.Width) / 2.0f), (int)(y + (Session.Map.SmallErrorButtonTexture.Height / 2.0f)));

                Vector2 tpos = new Vector2((int)(bpos.X - Session.Map.SmallErrorButtonTexture.Width / 2.0f + padding),
                    (int)(y + (Session.Map.SmallErrorButtonTexture.Height - btdim.Y) / 2.0f));

                Button b = new Button(Session.Map.SmallErrorButtonTexture, bpos, new Text(bt, spriteFont, tpos), Session.Map.ErrorColour, clickedTurret);
                b.Deactivate();
                SelectedTurret.Add("BuyTurret", b);
            }*/
        }

        void sellTurret_LeftClick(object sender, EventArgs e)
        {
            /*Button b = sender as Button;
            if (b != null)
            {
                Turret t = b.StoredObject as Turret;
                Session.SellTurret(t);
                clickedTurret = null;
            }*/
        }

        void upgradeTurret_LeftClick(object sender, EventArgs e)
        {
            /*Button b = sender as Button;
            if (b != null)
            {
                Turret t = b.StoredObject as Turret;
                Session.UpgradeTurret(t);
                b.ButtonText.Value = String.Format("Upgrade Turret (Costs {0})", clickedTurret.UpgradeCost);
                SelectedTurret.GetButton("SellTurret").ButtonText.Value = String.Format("Sell Turret (Receive {0})", (int)(clickedTurret.TotalCost * clickedTurret.SellScalar));
                SelectedTurret.GetText("Stats").Value = clickedTurret.CurrentStatistics.ToShortString();
                SelectedTurret.GetText("Price").Value = String.Format("Price: {0}", clickedTurret.TotalCost);
                SelectedTurret.GetText("TurretName").Value = clickedTurret.Name + " " + (clickedTurret.Level + 1).ToString();

                if (clickedTurret.UpgradeCost > Session.ActivePlayer.Money || clickedTurret.Level == clickedTurret.MaxLevel)
                {
                    b.Texture = Session.Map.SmallErrorButtonTexture;
                    b.SetColour(Session.Map.ErrorColour);
                    b.LeftClickEvent -= upgradeTurret_LeftClick;
                    b.Deactivate();
                }
            }*/
        }

        void buyTurret_LeftClick(object sender, EventArgs e)
        {
            Button b = sender as Button;
            if (b != null)
            {
                Turret t = b.StoredObject as Turret;
                if (t != null)
                {
                    // Session.SelectTurret(t);
                }
            }
        }

        void cancelButton_LeftClick(object sender, EventArgs e)
        {
            ResetTurretReferences();
            Session.UI.MapRegion.ResetTurretReferences();
        }

        private void InitializeStatsAndControls()
        {
            int y = StatsAndControls.Dimensions.Top + padding;
            StatsAndControls.Add("Wave", new Text(String.Format("Wave {0} of {1}", waveindex+1, Session.Map.WaveList.Count), new Vector2(StatsAndControls.Dimensions.Left + padding, y)));

            string bt = "Launch Next Wave Now";
            Vector2 btdim = spriteFont.MeasureString(bt);
            Texture2D tex = Session.Map.State == MapState.WaveDelay ? Session.Map.SmallNormalButtonTexture : Session.Map.SmallErrorButtonTexture;
            Color c = Session.Map.State == MapState.WaveDelay ? Session.Map.ForeColour : Session.Map.ErrorColour;

            Vector2 bpos = new Vector2((int)(MoneyAndTurrets.Dimensions.Left + (tex.Width / 2.0f) +
                (MoneyAndTurrets.Dimensions.Width - tex.Width) / 2.0f), (int)(y + (tex.Height / 2.0f)));

            Vector2 tpos = new Vector2((int)(bpos.X - tex.Width / 2.0f + padding),
                (int)(y + (tex.Height - btdim.Y) / 2.0f));

            Button b = new Button(tex, bpos, new Text(bt, spriteFont, tpos), c, clickedShip);
            b.LeftClickEvent += new EventHandler(nextWave_LeftClick);
            StatsAndControls.Add("LaunchNextWave", b);

            y += tex.Height + padding;

            tex = Session.Map.LargeNormalButtonTexture;
            int x = (int)(MoneyAndTurrets.Dimensions.Left + (tex.Width / 2.0f) + padding);
            c = Session.Map.ForeColour;
            bt = "Pause";
            btdim = spriteFont.MeasureString(bt);
            bpos = new Vector2(x, (int)(y + (tex.Height / 2.0f)));

            tpos = new Vector2((int)(bpos.X - tex.Width / 2.0f + padding),
                (int)(y + (tex.Height - btdim.Y) / 2.0f));

            b = new Button(tex, bpos, new Text(bt, spriteFont, tpos), c, null);
            b.LeftClickEvent += new EventHandler(pause_LeftClick);
            StatsAndControls.Add("Pause", b);

            x += tex.Width;
            bt = "Increase\nSpeed";
            btdim = spriteFont.MeasureString(bt);
            bpos = new Vector2(x, (int)(y + (tex.Height / 2.0f)));

            tpos = new Vector2((int)(bpos.X - tex.Width / 2.0f + padding),
                (int)(y + (tex.Height - btdim.Y) / 2.0f));

            b = new Button(tex, bpos, new Text(bt, spriteFont, tpos), c, null);
            b.LeftClickEvent += new EventHandler(increaseSpeed_LeftClick);
            StatsAndControls.Add("IncreaseSpeed", b);

            x += tex.Width;
            bt = "Decrease\nSpeed";
            btdim = spriteFont.MeasureString(bt);
            bpos = new Vector2(x, (int)(y + (tex.Height / 2.0f)));

            tpos = new Vector2((int)(bpos.X - tex.Width / 2.0f + padding),
                (int)(y + (tex.Height - btdim.Y) / 2.0f));

            b = new Button(tex, bpos, new Text(bt, spriteFont, tpos), c, null);
            b.LeftClickEvent += new EventHandler(decreaseSpeed_LeftClick);
            StatsAndControls.Add("DecreaseSpeed", b);
        }

        void pause_LeftClick(object sender, EventArgs e)
        {
            Button b = sender as Button;
            if (b.ButtonText.Value.Equals("Pause"))
            {
                Session.Pause();
            }
            else
            {
                Session.Resume();
            }
        }

        void increaseSpeed_LeftClick(object sender, EventArgs e)
        {
            if (Session.Speed < Session.MaxSpeed)
            {
                Button b = sender as Button;
                Session.IncreaseSpeed(0.1f);
            }
        }

        void decreaseSpeed_LeftClick(object sender, EventArgs e)
        {
            if (Session.Speed > Session.MinSpeed)
            {
                Button b = sender as Button;
                Session.DecreaseSpeed(0.1f);
            }
        }

        void nextWave_LeftClick(object sender, EventArgs e)
        {
            if (Session.Map.State == MapState.WaveDelay)
            {
                Session.Map.StartNextWaveNow();
            }
        }

        private void ResetTurretReferences()
        {
            if (clickedShip != null) clickedShip = null;
            if (Session.SelectedShip != null) Session.DeselectShip();
        }

        public void Update(GameTime gameTime)
        {
            // MoneyAndTurrets.GetText("Money").Value = Session.MoneyDisplay;
            // MoneyAndTurrets.GetText("Turrets").Value = Session.TurretsDisplay;
            Button lnw = StatsAndControls.GetButton("LaunchNextWave");
            Texture2D tex = Session.Map.State == MapState.WaveDelay ? Session.Map.SmallNormalButtonTexture
                : Session.Map.SmallErrorButtonTexture;
            Color c = Session.Map.State == MapState.WaveDelay ? Session.Map.ForeColour : Session.Map.ErrorColour;
            lnw.Texture = tex;
            lnw.SetColour(c);

            if (clickedShip != null)
            {
                foreach (var b in SelectedShip.Buttons)
                {
                    b.Value.Update(gameTime, Session.UI.Mouse);
                }
            }
            else
            {
                /*foreach (var b in PurchaseTurret.Buttons)
                {
                    b.Value.Update(gameTime, Session.UI.Mouse);
                }*/
            }

            foreach (var b in StatsAndControls.Buttons)
            {
                b.Value.Update(gameTime, Session.UI.Mouse);
            }

            Button isb = StatsAndControls.GetButton("IncreaseSpeed");
            Button dsb = StatsAndControls.GetButton("DecreaseSpeed");
            if (Session.Speed >= Session.MaxSpeed)
            {
                isb.Texture = Session.Map.LargeErrorButtonTexture;
                isb.SetColour(Session.Map.ErrorColour);
                dsb.Texture = Session.Map.LargeNormalButtonTexture;
                dsb.SetColour(Session.Map.ForeColour);
            }
            else if (Session.Speed <= Session.MinSpeed)
            {
                isb.Texture = Session.Map.LargeNormalButtonTexture;
                isb.SetColour(Session.Map.ForeColour);
                dsb.Texture = Session.Map.LargeErrorButtonTexture;
                dsb.SetColour(Session.Map.ErrorColour);
            }
            else
            {
                isb.Texture = Session.Map.LargeNormalButtonTexture;
                isb.SetColour(Session.Map.ForeColour);
                dsb.Texture = Session.Map.LargeNormalButtonTexture;
                dsb.SetColour(Session.Map.ForeColour);
            }

            if (waveindex != Session.Map.WaveIndex)
            {
                waveindex = Session.Map.WaveIndex;
                StatsAndControls.GetText("Wave").Value = String.Format("Wave {0} of {1}", waveindex + 1, Session.Map.WaveList.Count);
            }

            clickedShip = Session.ActivePlayer.PlayerShips[0];
            InitializeSelectedShip(clickedShip);
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch, SpriteFont spriteFont)
        {
            MoneyAndTurrets.Draw(gameTime, spriteBatch, spriteFont);

            if (clickedShip != null)
            {
                SelectedShip.Draw(gameTime, spriteBatch, spriteFont);
            }
            else
            {
                PurchaseShip.Draw(gameTime, spriteBatch, spriteFont);
            }

            StatsAndControls.Draw(gameTime, spriteBatch, spriteFont);
        }

        #endregion
    }
}
        