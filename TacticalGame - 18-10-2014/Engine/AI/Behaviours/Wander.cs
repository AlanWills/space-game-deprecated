﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Engine
{
    public class Wander : Action
    {
        float deltaangle, nextangle;
        Ship source;
        int threshold;
        Rectangle domain;

        public Wander(Ship owner, int threshold, Rectangle domain)
        {
            source = owner;
            this.threshold = threshold;
            this.domain = domain;
        }

        public override void Do()
        {
            if (source.Rectangle != Rectangle.Empty)
            {
                deltaangle = MathHelper.ToRadians(90);
                Vector2 adjustment = Vector2.Zero;

                if (source.Rectangle.Left < threshold)
                {
                    int distance = threshold - source.Rectangle.Left;
                    adjustment.X = distance;
                }
                else if (source.Rectangle.Right > (domain.Width - threshold))
                {
                    int distance = domain.Width - threshold - source.Rectangle.Right;
                    adjustment.X = distance;
                }

                if (source.Rectangle.Top < threshold)
                {
                    int distance = threshold - source.Rectangle.Top;
                    adjustment.Y = distance;
                }
                else if (source.Rectangle.Bottom > (domain.Height - threshold))
                {
                    int distance = domain.Height - threshold - source.Rectangle.Bottom;
                    adjustment.Y = distance;
                }

                if (adjustment != Vector2.Zero)
                {
                    source.Velocity += adjustment;
                    source.Velocity.Normalize();
                    source.Velocity *= source.Speed;
                    source.Rotation = (float)Math.Atan2(source.Velocity.Y, source.Velocity.X);

                    deltaangle *= -1;
                    nextangle = source.Rotation + deltaangle;
                }
                else if (Math.Abs(source.Rotation - nextangle) < 0.1f)
                {
                    deltaangle *= -1;
                    nextangle = source.Rotation + deltaangle;
                }
                else
                {
                    source.Rotation = MathHelper.Lerp(source.Rotation, nextangle, 0.01f);
                    source.Velocity = new Vector2(source.Speed * (float)Math.Cos(source.Rotation),
                        source.Speed * (float)Math.Sin(source.Rotation));
                }
            }
        }
    }
}
