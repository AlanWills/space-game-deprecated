﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Engine
{
    public class Seek : Action
    {
        Ship source, target;

        public Seek(Ship owner, Ship target)
        {
            source = owner;
            this.target = target;
        }

        public override void Do()
        {
            Vector2 sourceToTarget = Vector2.Subtract(target.Position, source.Position);
            sourceToTarget.Normalize();
            sourceToTarget = Vector2.Multiply(sourceToTarget, source.Speed);

            source.Velocity = sourceToTarget;
            source.Rotation = (float)Math.Atan2(-sourceToTarget.Y, sourceToTarget.X) + (float)Math.PI;
        }
    }
}
