﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Engine
{
    public class Flee : Action
    {
        Ship source, target;
        float distanceThreshold;

        public Flee(Ship owner, Ship target, float dt)
        {
            source = owner;
            this.target = target;
            distanceThreshold = dt;
        }

        public override void Do()
        {
            Vector2 sourceToTarget = Vector2.Subtract(source.Position, target.Position);

            if (sourceToTarget.Length() < distanceThreshold)
            {
                sourceToTarget.Normalize();
                sourceToTarget = Vector2.Multiply(sourceToTarget, source.Speed);

                source.Velocity = sourceToTarget;
                source.Rotation = (float)Math.Atan2(-sourceToTarget.Y, sourceToTarget.X) + (float)Math.PI;
            }

            else
            {
                if (source.Velocity != Vector2.Zero) source.Velocity = Vector2.Zero;
            }
        }
    }
}
