﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Engine
{
    public class ObstacleAvoidance : Action
    {
        Ship source;
        GameplayObject obstacle;
        int threshold;

        public ObstacleAvoidance(Ship owner, GameplayObject obstacle, int threshold)
        {
            source = owner;
            this.obstacle = obstacle;
            this.threshold = threshold;
        }

        public override void Do()
        {
            Vector2 distance = obstacle.Position - source.Position;
            float length = distance.Length();

            if (length < threshold)
            {
                Vector2 adjustment = Vector2.Zero;
                float diff = threshold - length;
                adjustment = new Vector2(distance.X, distance.Y);
                adjustment.Normalize();
                adjustment *= diff;
                source.Velocity -= adjustment;
                source.Velocity.Normalize();
                source.Velocity *= source.Speed;
                source.Rotation = (float)Math.Atan2(-source.Velocity.Y, source.Velocity.X) + (float)Math.PI;
            }
        }
    }
}
