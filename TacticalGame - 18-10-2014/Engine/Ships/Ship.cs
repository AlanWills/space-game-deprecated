﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Engine
{
    public class Ship : GameplayObject
    {
        #region Writer

        [ContentTypeWriter]
        public class ShipWriter : ContentTypeWriter<Ship>
        {
            GameplayObjectWriter gameplayObjectWriter = null;

            protected override void Initialize(ContentCompiler compiler)
            {
                gameplayObjectWriter = compiler.GetTypeWriter(typeof(GameplayObject)) as GameplayObjectWriter;
                
                base.Initialize(compiler);
            }

            protected override void Write(ContentWriter output, Ship value)
            {
                output.WriteRawObject<GameplayObject>(value as GameplayObject, gameplayObjectWriter);
                output.Write(value.Health);
                output.Write(value.TextureAsset);
                output.Write(value.Level);
                output.Write(value.MaxLevel);
                output.WriteObject(value.InitialStatistics);
                output.WriteObject(value.UpgradeStatistics);
                output.WriteObject(value.StartingTurrets);
                output.Write(value.HitCueName);
            }

            public override string GetRuntimeReader(Microsoft.Xna.Framework.Content.Pipeline.TargetPlatform targetPlatform)
            {
                return typeof(Ship.ShipReader).AssemblyQualifiedName;
            }
        }

        #endregion

        #region Properties and Fields

        [ContentSerializer]
        public int Health
        {
            get;
            protected set;
        }

        [ContentSerializerIgnore]
        public int MaxHealth
        {
            get;
            protected set;
        }

        [ContentSerializer]
        protected string TextureAsset
        {
            get;
            set;
        }

        int level;
        [ContentSerializer]
        public int Level
        {
            get { return level; }
            set
            {
                if (value < MaxLevel)
                {
                    level = value;
                    LevelUp();
                }
            }
        }

        [ContentSerializer]
        public int MaxLevel
        {
            get;
            protected set;
        }

        ShipStatistics initialStatistics;
        [ContentSerializer]
        public ShipStatistics InitialStatistics
        {
            get { return initialStatistics; }
            set
            {
                initialStatistics = value;
                LevelUp();
            }
        }

        [ContentSerializerIgnore]
        public ShipStatistics CurrentStatistics
        {
            get;
            protected set;
        }

        ShipUpgradeStatistics upgradeStatistics;
        [ContentSerializer]
        public ShipUpgradeStatistics UpgradeStatistics
        {
            get { return upgradeStatistics; }
            set
            {
                upgradeStatistics = value;
                LevelUp();
            }
        }

        [ContentSerializer]
        public List<string> StartingTurrets
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public List<Turret> Turrets
        {
            get;
            protected set;
        }

        [ContentSerializerIgnore]
        public Wave Wave
        {
            get;
            set;
        }

        [ContentSerializerIgnore]
        public Map ActiveMap
        {
            get;
            set;
        }

        [ContentSerializer]
        public string HitCueName
        {
            get;
            protected set;
        }

        [ContentSerializerIgnore]
        public ObjectClickedState LeftState
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public Mouse ActiveMouse
        {
            get;
            protected set;
        }

        public event EventHandler LeftClickEvent;

        [ContentSerializerIgnore]
        public bool IsActive
        {
            get { return Status == ObjectStatus.Active; }
        }

        [ContentSerializerIgnore]
        public Ship Target
        {
            get;
            protected set;
        }

        [ContentSerializerIgnore]
        public Queue<Action> AvailableBehaviours
        {
            get;
            private set;
        }

        float hitTimer;
        float statisticsTimer;

        Text hitDisplay;

        float deltaHealth;
        float DoTTimer;
        float DoTTime;
        int dotHealth;

        public event EventHandler DieEvent;
        public event EventHandler HitEvent;

        #endregion

        #region Constructor

        public Ship()
        {
            StartingTurrets = new List<string>();
            Turrets = new List<Turret>();
            AvailableBehaviours = new Queue<Action>();
        }

        #endregion

        #region Methods

        public void LevelUp()
        {
            if (level > 0 && InitialStatistics != null && upgradeStatistics != null)
            {
                CurrentStatistics = ShipStatistics.Add(InitialStatistics, ShipUpgradeStatistics.Multiply(upgradeStatistics, level));
            }
        }

        #endregion

        #region Update and Draw

        public override void Update(GameTime gameTime)
        {
            if (dotHealth > 0 && DoTTime > 0)
            {
                UpdateDoT(gameTime);

                if (DoTTimer > DoTTime)
                {
                    deltaHealth = 0.0f;
                    dotHealth = 0;
                    DoTTime = 0;
                    DoTTimer = 0;
                }
            }

            if (hitTimer > 0)
            {
                hitTimer -= (float)gameTime.ElapsedGameTime.TotalSeconds * Session.singleton.Speed;
                hitDisplay.Update(gameTime);

                if (hitTimer <= 0)
                {
                    hitDisplay = null;
                }
            }

            UpdateMouse(gameTime, Session.singleton.UI.Mouse);

            foreach (Turret t in Turrets)
            {
                t.Owner = this;
                t.Update(gameTime);
            }

            base.Update(gameTime);
        }

        private void UpdateMouse(GameTime gameTime, Mouse activeMouse)
        {
            bool intersect = activeMouse.Rectangle.Intersects(Rectangle);

            if (intersect || (ActiveMouse != null && (LeftState != ObjectClickedState.Normal)))
            {
                ActiveMouse = activeMouse;

                if (ActiveMouse.LeftClick && (LeftState == ObjectClickedState.Normal || LeftState == ObjectClickedState.Released))
                {
                    LeftState = ObjectClickedState.Clicked;

                    if (LeftClickEvent != null)
                        LeftClickEvent(this, EventArgs.Empty);
                }
                else if (ActiveMouse.LeftRelease && LeftState == ObjectClickedState.Clicked)
                {
                    LeftState = ObjectClickedState.Normal;
                }
            }

            else
            {
                if (ActiveMouse != null)
                {
                    ActiveMouse = null;
                }
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            Texture2D t = Texture;

            base.Draw(gameTime, spriteBatch);

            if (hitTimer > 0 && hitDisplay != null) hitDisplay.Draw(spriteBatch, Session.singleton.UI.Font, Session.singleton.Map.ForeColour);
            Texture = t;

            foreach (Turret tur in Turrets)
            {
                tur.Draw(gameTime, spriteBatch);
            }
        }

        #endregion

        #region Private Update Methods

        private void UpdateDoT(GameTime gameTime)
        {
            float deltaSeconds = (float)gameTime.ElapsedGameTime.TotalSeconds * Session.singleton.Speed;

            DoTTimer += deltaSeconds;
            deltaHealth += dotHealth * deltaSeconds;

            int damage = (int)deltaHealth;

            if (damage > 0)
            {
                Health -= damage;
                deltaHealth = deltaHealth - damage;
            }

            if (Health <= 0)
            {
                Wave.Remove(this);
                Die();
                Session.singleton.AddMoney(Wave.MoneyPerKill);

                if (DieEvent != null)
                    DieEvent(this, EventArgs.Empty);
            }
            else
            {
                hitTimer = 0.2f;
                hitDisplay = new Text(damage.ToString(), new Vector2(Rectangle.Right + 3, Rectangle.Top), new Vector2(Velocity.X, -Speed));
            }
        }

        internal void ApplyDoT(int damage, float time)
        {
            if (dotHealth == 0 && DoTTime == 0)
            {
                deltaHealth = 0;
                dotHealth = damage;
                DoTTime = time;
                DoTTimer = 0;
            }
        }

        public void Hit(Bullet bullet, Turret owner)
        {
            AudioManager.singleton.PlaySound(HitCueName);
            Health -= owner.CurrentStatistics.Damage;

            if (Health <= 0)
            {
                Wave.Remove(this);
                Die();
                Session.singleton.AddMoney(Wave.MoneyPerKill);
                if (DieEvent != null)
                    DieEvent(this, EventArgs.Empty);
            }
            else
            {
                hitTimer = 0.4f;
                hitDisplay = new Text(
                    owner.CurrentStatistics.Damage.ToString(),
                    new Vector2(Rectangle.Right + 3, Rectangle.Top),
                    new Vector2(Velocity.X, -Speed));
            }
        }

        #endregion

        #region Reader

        public class ShipReader : ContentTypeReader<Ship>
        {
            protected override Ship Read(ContentReader input, Ship existingInstance)
            {
                Ship result = existingInstance;

                if (result == null) result = new Ship();

                input.ReadRawObject<GameplayObject>(result as GameplayObject);
                result.Health = input.ReadInt32();
                result.MaxHealth = result.Health;
                result.TextureAsset = input.ReadString();
                result.Texture = input.ContentManager.Load<Texture2D>(String.Format("Textures\\Ships\\{0}", result.TextureAsset));
                result.Level = input.ReadInt32();
                if (result.Level > 0) result.Name += " " + result.Level.ToString();
                result.MaxLevel = input.ReadInt32();
                result.InitialStatistics = input.ReadObject<ShipStatistics>();
                result.CurrentStatistics = result.InitialStatistics;
                result.UpgradeStatistics = input.ReadObject<ShipUpgradeStatistics>();
                result.StartingTurrets.AddRange(input.ReadObject<List<string>>());
                result.Turrets = new List<Turret>(result.StartingTurrets.Count);

                Turret t = null;
                foreach (string s in result.StartingTurrets)
                {
                    t = input.ContentManager.Load<Turret>(String.Format("Turrets\\{0}", s)).Clone();
                    t.Initialize(result, input.ContentManager);
                    result.Turrets.Add(t);
                }

                result.HitCueName = input.ReadString();
                
                return result;
            }
        }

        #endregion

        internal Ship Clone()
        {
            Ship s = new Ship();

            s.Name = Name;
            s.Description = Description;
            s.Alpha = Alpha;
            s.Speed = Speed;
            s.Health = Health;
            s.MaxHealth = MaxHealth;
            s.TextureAsset = TextureAsset;
            s.Texture = Texture;
            s.InitialStatistics = InitialStatistics;
            s.CurrentStatistics = CurrentStatistics;
            s.HitCueName = HitCueName;
            s.Turrets = Turrets;

            return s;
        }
    }
}
