﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Engine
{
    public class Enemy : Ship
    {
        #region Writer

        [ContentTypeWriter]
        public class EnemyWriter : ContentTypeWriter<Enemy>
        {
            ShipWriter shipWriter = null;

            protected override void Initialize(ContentCompiler compiler)
            {
                shipWriter = compiler.GetTypeWriter(typeof(Ship)) as ShipWriter;
                
                base.Initialize(compiler);
            }

            protected override void Write(ContentWriter output, Enemy value)
            {
                output.WriteRawObject<Ship>(value as Ship, shipWriter);
                output.Write(value.Delay);
            }

            public override string GetRuntimeReader(Microsoft.Xna.Framework.Content.Pipeline.TargetPlatform targetPlatform)
            {
                return typeof(Enemy.EnemyReader).AssemblyQualifiedName;
            }
        }


        #endregion

        #region Properties

        [ContentSerializerIgnore]
        public float DistanceToTravel
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public float DistanceTravelled
        {
            get;
            private set;
        }

        [ContentSerializer]
        public float Delay
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public Enemy()
        {
            DistanceToTravel = float.MinValue;
            DistanceTravelled = float.MaxValue;
        }

        #endregion

        #region Methods

        public override void Update(GameTime gameTime)
        {
            // Handle specific enemy movement
            Target = FindAllyTarget();

            if (Target != null)
            {
                AvailableBehaviours.Enqueue(new Flee(this, Target, 500));
                AvailableBehaviours.Dequeue().Do();
            }

            base.Update(gameTime);
        }


        protected Ally FindAllyTarget()
        {
            Ally a = null;
            float distance = 0, newDistance = 0;

            List<Ally> playerShips = Session.singleton.ActivePlayer.PlayerShips;

            if (playerShips.Count > 0)
            {
                a = playerShips[0];
                distance = Vector2.DistanceSquared(a.Position, Position);

                for (int i = 1; i < playerShips.Count; i++)
                {
                    newDistance = Vector2.DistanceSquared(playerShips[i].Position, Position);

                    if (newDistance < distance)
                    {
                        distance = newDistance;
                        a = playerShips[i];
                    }
                }
            }

            return a;
        }


        public void AddToWave(Wave w) 
        {
            Wave = w;
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);
        }

        #endregion

        #region Enemy Reader

        public class EnemyReader : ContentTypeReader<Enemy>
        {
            protected override Enemy Read(ContentReader input, Enemy existingInstance)
            {
                Enemy result = new Enemy();

                input.ReadRawObject<Ship>(result as Ship);
                result.Delay = input.ReadSingle();

                return result;
            }
        }

        #endregion

        internal Enemy Clone()
        {
            Enemy s = new Enemy();

            s.Name = Name;
            s.Description = Description;
            s.Alpha = Alpha;
            s.Speed = Speed;
            s.Health = Health;
            s.MaxHealth = MaxHealth;
            s.TextureAsset = TextureAsset;
            s.Texture = Texture;
            s.InitialStatistics = InitialStatistics;
            s.CurrentStatistics = CurrentStatistics;
            s.HitCueName = HitCueName;
            s.Turrets = Turrets;
            s.Delay = Delay;

            return s;
        }
    }
}
