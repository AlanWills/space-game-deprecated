﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Engine
{
    public class Turret
    {
        #region Writer

        [ContentTypeWriter]
        public class TurretWriter : ContentTypeWriter<Turret>
        {
            protected override void Write(ContentWriter output, Turret value)
            {
                output.Write(value.Name);
                output.Write(value.Description);
                output.Write(value.ThumbnailAsset);
                output.Write(value.TextureAsset);
                output.Write(value.Cost);
                output.Write(value.Level);
                output.Write(value.MaxLevel);
                output.WriteObject(value.InitialStatistics);
                output.WriteObject(value.UpgradeStatistics);
                output.Write(value.BulletAsset);
                output.Write(value.SellScalar);
            }

            public override string GetRuntimeReader(Microsoft.Xna.Framework.Content.Pipeline.TargetPlatform targetPlatform)
            {
                return typeof(Turret.TurretReader).AssemblyQualifiedName;
            }
        }

        #endregion

        #region Properties

        [ContentSerializer]
        public string Name
        {
            get;
            private set;
        }

        [ContentSerializer]
        public string Description
        {
            get;
            private set;
        }

        [ContentSerializer]
        public string ThumbnailAsset
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public Texture2D Thumbnail
        {
            get;
            private set;
        }

        [ContentSerializer]
        public string TextureAsset
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public Texture2D Texture
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public Vector2 Origin
        {
            get;
            private set;
        }

        [ContentSerializer]
        public int Cost
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public int UpgradeCost
        {
            get { return ((Level + 1) * Cost) / 2; }
        }

        [ContentSerializerIgnore]
        public int TotalCost
        {
            get;
            private set;
        }

        int level;
        [ContentSerializer]
        public int Level
        {
            get { return level; }
            set
            {
                if (value < MaxLevel)
                {
                    level = value;
                    LevelUp();
                }
            }
        }

        [ContentSerializer]
        public int MaxLevel
        {
            get;
            private set;
        }

        TurretStatistics initialStatistics;
        [ContentSerializer]
        public TurretStatistics InitialStatistics
        {
            get { return initialStatistics; }
            set
            {
                initialStatistics = value;
                LevelUp();
            }
        }

        [ContentSerializerIgnore]
        public TurretStatistics CurrentStatistics
        {
            get;
            private set;
        }

        UpgradeStatistics upgradeStatistics;
        [ContentSerializer]
        public UpgradeStatistics UpgradeStatistics
        {
            get { return upgradeStatistics; }
            set
            {
                upgradeStatistics = value;
                LevelUp();
            }
        }

        [ContentSerializerIgnore]
        public Ship Owner
        {
            get;
            set;
        }

        [ContentSerializer]
        private string BulletAsset
        {
            get;
            set;
        }

        [ContentSerializerIgnore]
        public Bullet BulletBase
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public List<Bullet> Bullets
        {
            get;
            private set;
        }

        [ContentSerializer]
        public float SellScalar
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public Rectangle Rectangle
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public Vector2 Position
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public float Rotation
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public bool Initialized
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public bool IsPlaced
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public Ship Target
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public float PlacedTime
        {
            get;
            private set;
        }

        float timer;
        public event EventHandler LeftClickEvent;

        #endregion

        #region Methods

        public void Initialize(Ship ship)
        {
            Owner = ship;
            Initialized = true;
            Bullets = new List<Bullet>();
        }

        public void Initialize(Ship ship, ContentManager contentManager)
        {
            Owner = ship;

            if (!string.IsNullOrEmpty(ThumbnailAsset))
            {
                Thumbnail = contentManager.Load<Texture2D>(String.Format("Textures\\Turrets\\{0}", ThumbnailAsset));
            }

            if (!string.IsNullOrEmpty(TextureAsset))
            {
                Texture = contentManager.Load<Texture2D>(String.Format("Textures\\Turrets\\{0}", TextureAsset));
                Origin = new Vector2(Texture.Width / 2, Texture.Height / 2);
            }

            BulletBase.Initialize(contentManager);

            timer = CurrentStatistics.Speed;

            Initialized = true;
            Bullets = new List<Bullet>();
        }

        private void LevelUp()
        {
            if (level > 0 && initialStatistics != null && upgradeStatistics != null)
            {
                CurrentStatistics = TurretStatistics.Add(InitialStatistics, UpgradeStatistics.Multiply(upgradeStatistics, level));
            }
        }

        public void Upgrade()
        {
            TotalCost += UpgradeCost;
            Level++;
        }

        public void Update(GameTime gameTime)
        {
            // Updates position of the turret
            Position = Owner.Position;

            if (IsPlaced) PlacedTime += (float)(gameTime.ElapsedGameTime.TotalSeconds * Session.singleton.Speed);
            if (timer > 0)
            {
                timer -= (float)(gameTime.ElapsedGameTime.TotalSeconds * Session.singleton.Speed);
            }

            foreach (Bullet b in Bullets)
            {
                b.Update(gameTime);
            }

            if (Target != null)
            {
                UpdateTarget(gameTime);
            }
            else
            {
                Target = FindTarget();
            }
        }

        private void UpdateTarget(GameTime gameTime)
        {
            float distance = Vector2.Distance(Target.Position, Position);
            if (distance <= CurrentStatistics.Radius && Target.IsActive)
            {
                float rate = BulletBase.Speed;
                Vector2 d = Vector2.Add(Target.Position, Vector2.Multiply(Target.Velocity, distance / rate)) - Position;
                Rotation = (float)Math.Atan2(-d.Y, d.X);

                if (timer <= 0)
                {
                    Bullet b = BulletBase.Clone();
                    b.Fire(this);
                    Bullets.Add(b);
                    timer = CurrentStatistics.Speed;
                }
            }
            else
            {
                Target.DieEvent -= new EventHandler(m_DieEvent);
                Target = null;
            }
        }

        private Ship FindTarget()
        {
            Ship target = null;
            float distance = 0, newDistance = 0;

            Enemy e = Owner as Enemy;

            // If the turret is an ally turret we find the closest enemy, otherwise we find the closest ally.
            if (e == null)
            {
                Wave active = Session.singleton.Map.ActiveWave;

                if (active.Enemies.Count > 0)
                {
                    target = active.Enemies[0];
                    distance = Vector2.DistanceSquared(target.Position, Position);

                    for (int i = 1; i < active.Enemies.Count; i++)
                    {
                        newDistance = Vector2.DistanceSquared(active.Enemies[i].Position, Position);

                        if (newDistance < distance)
                        {
                            distance = newDistance;
                            target = active.Enemies[i];
                        }
                    }
                }
            }
            else
            {
                List<Ally> playerShips = Session.singleton.ActivePlayer.PlayerShips;

                if (playerShips.Count > 0)
                {
                    target = playerShips[0];
                    distance = Vector2.DistanceSquared(target.Position, Position);

                    for (int i = 1; i < playerShips.Count; i++)
                    {
                        newDistance = Vector2.DistanceSquared(playerShips[i].Position, Position);

                        if (newDistance < distance)
                        {
                            distance = newDistance;
                            target = playerShips[i];
                        }
                    }
                }

                return target;
            }

            // Check to see if the closest opponent ship is within range of our turret
            if ((Math.Sqrt(distance) <= CurrentStatistics.Radius)
                    && (target != null && target.IsActive))
            {
                target.DieEvent += new EventHandler(m_DieEvent);
                return target;
            }
            else
                return null;
        }

        void m_DieEvent(object sender, EventArgs e)
        {
            Target.Turrets.Clear();
            Target = null;
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            foreach (Bullet b in Bullets)
            {
                b.Draw(gameTime, spriteBatch);
            }

            spriteBatch.Draw(Texture, Position, null, Color.White, Rotation, Origin, 1.0f, SpriteEffects.None, 1.0f);
        }

        #endregion

        #region Reader

        public class TurretReader : ContentTypeReader<Turret>
        {
            protected override Turret Read(ContentReader input, Turret existingInstance)
            {
                Turret t = new Turret();

                t.Name = input.ReadString();
                t.Description = input.ReadString();
                t.ThumbnailAsset = input.ReadString();
                t.TextureAsset = input.ReadString();
                t.Cost = input.ReadInt32();
                t.Level = input.ReadInt32();
                if (t.Level > 0) t.Name += " " + t.Level.ToString();
                t.MaxLevel = input.ReadInt32();
                t.InitialStatistics = input.ReadObject<TurretStatistics>();
                t.CurrentStatistics = t.InitialStatistics;
                t.UpgradeStatistics = input.ReadObject<UpgradeStatistics>();
                t.BulletAsset = input.ReadString();
                t.BulletBase = input.ContentManager.Load<Bullet>(String.Format("Turrets\\Bullets\\{0}", t.BulletAsset)).Clone();
                t.SellScalar = input.ReadSingle();

                return t;
            }
        }

        #endregion

        internal Turret Clone()
        {
            Turret t = new Turret();

            t.Name = Name;
            t.Description = Description;
            t.ThumbnailAsset = ThumbnailAsset;
            t.Thumbnail = Thumbnail;
            t.TextureAsset = TextureAsset;
            t.Texture = Texture;
            t.Cost = Cost;
            t.Level = Level;
            t.MaxLevel = MaxLevel;
            t.InitialStatistics = InitialStatistics;
            t.CurrentStatistics = CurrentStatistics;
            t.UpgradeStatistics = UpgradeStatistics;
            t.BulletBase = BulletBase;
            t.SellScalar = SellScalar;
            t.Origin = Origin;
            t.Owner = Owner;

            return t;
        }
    }
}
