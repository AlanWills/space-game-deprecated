﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Engine
{
    public class Ally : Ship
    {
        #region Writer

        [ContentTypeWriter]
        public class AllyWriter : ContentTypeWriter<Ally>
        {
            ShipWriter shipWriter = null;

            protected override void Initialize(ContentCompiler compiler)
            {
                shipWriter = compiler.GetTypeWriter(typeof(Ship)) as ShipWriter;
                
                base.Initialize(compiler);
            }

            protected override void Write(ContentWriter output, Ally value)
            {
                output.WriteRawObject<Ship>(value as Ship, shipWriter);
                output.Write(value.RadiusTextureAsset);
                output.Write(value.Cost);
                output.Write(value.SellScalar);
            }

            public override string GetRuntimeReader(Microsoft.Xna.Framework.Content.Pipeline.TargetPlatform targetPlatform)
            {
                return typeof(Ally.AllyReader).AssemblyQualifiedName; 
            }
        }

        #endregion

        #region Properties and Fields

        [ContentSerializer]
        public string RadiusTextureAsset
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public Texture2D RadiusTexture
        {
            get;
            private set;
        }

        [ContentSerializer]
        public int Cost
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public int UpgradeCost
        {
            get { return ((Level + 1) * Cost) / 2; }
        }

        [ContentSerializerIgnore]
        public int TotalCost
        {
            get;
            private set;
        }

        [ContentSerializer]
        public float SellScalar
        {
            get;
            private set;
        }
        
        #endregion

        #region Constructor

        public Ally()
        {

        }

        #endregion

        #region Initialize

        public void Initialize(ContentManager content)
        {
            if (!string.IsNullOrEmpty(RadiusTextureAsset))
            {
                RadiusTexture = content.Load<Texture2D>(String.Format("Textures\\Maps\\RadiusTextures\\{0}", RadiusTextureAsset));
            }
        }

        #endregion

        #region Methods

        public void Upgrade()
        {
            TotalCost += UpgradeCost;
            Level++;
        }

        #endregion

        #region Update and Draw Methods

        public void DrawRadius(GameTime gameTime, SpriteBatch spriteBatch)
        {
            // radius = 
            /*float scale = (CurrentStatistics.Radius) / (float)RadiusTexture.Width;

            spriteBatch.Draw(
                RadiusTexture,
                Position,
                null,
                Color.White,
                0.0f,
                new Vector2(RadiusTexture.Width / 2, RadiusTexture.Height / 2),
                scale * 2,
                SpriteEffects.None,
                1.0f);*/
        }

        public override void Update(GameTime gameTime)
        {
            Target = FindEnemyTarget();

            if (Target != null)
            {
                AvailableBehaviours.Enqueue(new Seek(this, Target));
                AvailableBehaviours.Enqueue(new ObstacleAvoidance(this, Target, 500));
                AvailableBehaviours.Dequeue().Do();
                AvailableBehaviours.Dequeue().Do();
            }

            base.Update(gameTime);
        }

        protected Enemy FindEnemyTarget()
        {
            Wave active = Session.singleton.Map.ActiveWave;
            Enemy m = null;
            float distance = 0, newDistance = 0;

            if (active.Enemies.Count > 0)
            {
                m = active.Enemies[0];
                distance = Vector2.DistanceSquared(m.Position, Position);

                for (int i = 1; i < active.Enemies.Count; i++)
                {
                    newDistance = Vector2.DistanceSquared(active.Enemies[i].Position, Position);

                    if (newDistance < distance)
                    {
                        distance = newDistance;
                        m = active.Enemies[i];
                    }
                }
            }

            return m;
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {   
            base.Draw(gameTime, spriteBatch);
        }

        #endregion

        #region Reader

        public class AllyReader : ContentTypeReader<Ally>
        {
            protected override Ally Read(ContentReader input, Ally existingInstance)
            {
                Ally result = new Ally();

                input.ReadRawObject<Ship>(result as Ship);
                result.RadiusTextureAsset = input.ReadString();
                result.RadiusTexture = input.ContentManager.Load<Texture2D>(String.Format("Textures\\{0}", result.RadiusTextureAsset));
                result.Cost = input.ReadInt32();
                result.TotalCost = result.Cost;
                result.SellScalar = input.ReadSingle();

                return result;
            }
        }

        #endregion

        internal Ally Clone()
        {
            Ally p = new Ally();

            p.Name = Name;
            p.Description = Description;
            p.Alpha = Alpha;
            p.Speed = Speed;
            p.Health = Health;
            p.MaxHealth = MaxHealth;
            p.TextureAsset = TextureAsset;
            p.Texture = Texture;
            p.InitialStatistics = InitialStatistics;
            p.CurrentStatistics = CurrentStatistics;
            p.HitCueName = HitCueName;
            p.Turrets = Turrets;
            p.RadiusTextureAsset = RadiusTextureAsset;
            p.RadiusTexture = RadiusTexture;
            p.Cost = Cost;
            p.TotalCost = TotalCost;
            p.SellScalar = SellScalar;

            return p;
        }
    }
}
