﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;

namespace Engine
{
    public class ShipStatistics
    {
        #region Writer

        [ContentTypeWriter]
        public class ShipStatisticsWriter : ContentTypeWriter<ShipStatistics>
        {
            protected override void Write(ContentWriter output, ShipStatistics value)
            {
                output.Write(value.Health);
                output.Write(value.Speed);
            }

            public override string GetRuntimeReader(Microsoft.Xna.Framework.Content.Pipeline.TargetPlatform targetPlatform)
            {
                return typeof(ShipStatistics.ShipStatisticsReader).AssemblyQualifiedName;
            }
        }

        #endregion

        #region Properties

        [ContentSerializer(Optional = true)]
        public int Health
        {
            get;
            set;
        }

        [ContentSerializer(Optional = true)]
        public float Speed
        {
            get;
            set;
        }

        #endregion

        #region Add, Subtract and Multiply

        public static ShipStatistics Add(ShipStatistics a, ShipStatistics b)
        {
            ShipStatistics result = new ShipStatistics();

            result.Health = a.Health + b.Health;
            result.Speed = a.Speed + b.Speed;

            return result;
        }

        public static ShipStatistics Add(ShipStatistics a, ShipUpgradeStatistics b)
        {
            ShipStatistics result = new ShipStatistics();

            result.Health = a.Health + b.HealthIncrease;
            result.Speed = a.Speed + b.SpeedIncrease;

            return result;
        }

        public static ShipStatistics Subtract(ShipStatistics a, ShipStatistics b)
        {
            ShipStatistics result = new ShipStatistics();

            result.Health = a.Health - b.Health;
            result.Speed = a.Speed - b.Speed;

            return result;
        }

        public static ShipStatistics Multiply(ShipStatistics a, float b)
        {
            ShipStatistics result = new ShipStatistics();

            result.Health = (int)(a.Health * b);
            result.Speed = a.Speed * b;

            return result;
        }

        #endregion

        #region Methods

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(string.Format("[H: {0}", Health));
            sb.Append(string.Format("; FR: {0}", Speed));

            return sb.ToString();
        }

        public string ToShortString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(string.Format("Stats: [H: {0}", Health));
            sb.Append(string.Format("; FR: {0}]", Speed));

            return sb.ToString();
        }

        #endregion

        #region Reader

        public class ShipStatisticsReader : ContentTypeReader<ShipStatistics>
        {
            protected override ShipStatistics Read(ContentReader input, ShipStatistics existingInstance)
            {
                ShipStatistics result = new ShipStatistics();

                result.Health = input.ReadInt32();
                result.Speed = input.ReadSingle();

                return result;
            }
        }

        #endregion
    }
}
