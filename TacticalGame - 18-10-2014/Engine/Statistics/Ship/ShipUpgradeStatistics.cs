﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;

namespace Engine
{
    public class ShipUpgradeStatistics
    {
        #region Writer

        [ContentTypeWriter]
        public class ShipUpgradeStatisticsWriter : ContentTypeWriter<ShipUpgradeStatistics>
        {
            protected override void Write(ContentWriter output, ShipUpgradeStatistics value)
            {
                output.Write(value.HealthIncrease);
                output.Write(value.SpeedIncrease);
            }

            public override string GetRuntimeReader(Microsoft.Xna.Framework.Content.Pipeline.TargetPlatform targetPlatform)
            {
                return typeof(ShipUpgradeStatistics.ShipUpgradeStatisticsReader).AssemblyQualifiedName;
            }
        }

        #endregion

        #region Properties

        [ContentSerializer(Optional = true)]
        public int HealthIncrease
        {
            get;
            set;
        }

        [ContentSerializer(Optional = true)]
        public float SpeedIncrease
        {
            get;
            set;
        }

        #endregion

        #region Add, Subtract and Multiply

        public static ShipUpgradeStatistics Add(ShipUpgradeStatistics a, ShipUpgradeStatistics b)
        {
            ShipUpgradeStatistics result = new ShipUpgradeStatistics();

            result.HealthIncrease = a.HealthIncrease + b.HealthIncrease;
            result.SpeedIncrease = a.SpeedIncrease + b.SpeedIncrease;

            return result;
        }

        public static ShipUpgradeStatistics Subtract(ShipUpgradeStatistics a, ShipUpgradeStatistics b)
        {
            ShipUpgradeStatistics result = new ShipUpgradeStatistics();

            result.HealthIncrease = a.HealthIncrease - b.HealthIncrease;
            result.SpeedIncrease = a.SpeedIncrease - b.SpeedIncrease;

            return result;
        }

        public static ShipUpgradeStatistics Multiply(ShipUpgradeStatistics a, float b)
        {
            ShipUpgradeStatistics result = new ShipUpgradeStatistics();

            result.HealthIncrease = (int)(a.HealthIncrease * b);
            result.SpeedIncrease = a.SpeedIncrease * b;

            return result;
        }

        #endregion

        #region Reader

        public class ShipUpgradeStatisticsReader : ContentTypeReader<ShipUpgradeStatistics>
        {
            protected override ShipUpgradeStatistics Read(ContentReader input, ShipUpgradeStatistics existingInstance)
            {
                ShipUpgradeStatistics result = new ShipUpgradeStatistics();

                result.HealthIncrease = input.ReadInt32();
                result.SpeedIncrease = input.ReadSingle();

                return result;
            }
        }

        #endregion
    }
}
