﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;

namespace Engine
{
    public class TurretStatistics
    {
        #region Writer

        [ContentTypeWriter]
        public class TurretStatisticsWriter : ContentTypeWriter<TurretStatistics>
        {
            protected override void Write(ContentWriter output, TurretStatistics value)
            {
                output.Write(value.Health);
                output.Write(value.Damage);
                output.Write(value.Speed);
                output.Write(value.Accuracy);
                output.Write(value.CriticalChance);
                output.Write(value.CriticalHitScalar);
                output.Write(value.Radius);
            }

            public override string GetRuntimeReader(Microsoft.Xna.Framework.Content.Pipeline.TargetPlatform targetPlatform)
            {
                return typeof(TurretStatistics.TurretStatisticsReader).AssemblyQualifiedName;
            }
        }

        #endregion

        #region Properties

        public int Health
        {
            get;
            set;
        }

        public int Damage
        {
            get;
            set;
        }

        public float Speed
        {
            get;
            set;
        }

        public float Accuracy
        {
            get;
            set;
        }

        public float CriticalChance
        {
            get;
            set;
        }

        public float CriticalHitScalar
        {
            get;
            set;
        }

        public int Radius
        {
            get;
            set;
        }

        #endregion

        #region Add, Subtract and Multiply

        public static TurretStatistics Add(TurretStatistics a, TurretStatistics b)
        {
            TurretStatistics result = new TurretStatistics();

            result.Health = a.Health + b.Health;
            result.Damage = a.Damage + b.Damage;
            result.Speed = a.Speed + b.Speed;
            result.Accuracy = a.Accuracy + b.Accuracy;
            result.CriticalChance = a.CriticalChance + b.CriticalChance;
            result.CriticalHitScalar = a.CriticalHitScalar + b.CriticalHitScalar;
            result.Radius = a.Radius + b.Radius;

            return result;
        }

        public static TurretStatistics Add(TurretStatistics a, UpgradeStatistics b)
        {
            TurretStatistics result = new TurretStatistics();

            result.Health = a.Health + b.HealthIncrease;
            result.Damage = a.Damage + b.DamageIncrease;
            result.Speed = a.Speed + b.SpeedIncrease;
            result.Accuracy = a.Accuracy + b.AccuracyIncrease;
            result.CriticalChance = a.CriticalChance + b.CriticalChanceIncrease;
            result.CriticalHitScalar = a.CriticalHitScalar + b.CriticalHitScalarIncrease;
            result.Radius = a.Radius + b.RadiusIncrease;

            return result;
        }

        public static TurretStatistics Subtract(TurretStatistics a, TurretStatistics b)
        {
            TurretStatistics result = new TurretStatistics();

            result.Health = a.Health - b.Health;
            result.Damage = a.Damage - b.Damage;
            result.Speed = a.Speed - b.Speed;
            result.Accuracy = a.Accuracy - b.Accuracy;
            result.CriticalChance = a.CriticalChance - b.CriticalChance;
            result.CriticalHitScalar = a.CriticalHitScalar - b.CriticalHitScalar;
            result.Radius = a.Radius - b.Radius;

            return result;
        }

        public static TurretStatistics Multiply(TurretStatistics a, float b)
        {
            TurretStatistics result = new TurretStatistics();

            result.Health = (int)(a.Health * b);
            result.Damage = (int)(a.Damage * b);
            result.Speed = a.Speed * b;
            result.Accuracy = a.Accuracy * b;
            result.CriticalChance = a.CriticalChance * b;
            result.CriticalHitScalar = a.CriticalHitScalar * b;
            result.Radius = (int)(a.Radius * b);

            return result;
        }

        #endregion

        #region Methods

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(string.Format("[H: {0}", Health));
            sb.Append(string.Format("; D: {0}", Damage));
            sb.Append(string.Format("; FR: {0}", Speed));
            sb.AppendLine(string.Format("; A: {0}]", Accuracy));

            sb.Append(string.Format("[CC: {0}", CriticalChance));
            sb.Append(string.Format("; CH: {0}", CriticalHitScalar));
            sb.Append(string.Format("; R: {0}]", Radius));

            return sb.ToString();
        }

        public string ToShortString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(string.Format("Stats: [H: {0}", Health));
            sb.Append(string.Format("; D: {0}", Damage));
            sb.Append(string.Format("; FR: {0}]", Speed));

            return sb.ToString();
        }

        #endregion

        #region Reader

        public class TurretStatisticsReader : ContentTypeReader<TurretStatistics>
        {
            protected override TurretStatistics Read(ContentReader input, TurretStatistics existingInstance)
            {
                TurretStatistics result = new TurretStatistics();

                result.Health = input.ReadInt32();
                result.Damage = input.ReadInt32();
                result.Speed = input.ReadSingle();
                result.Accuracy = input.ReadSingle();
                result.CriticalChance = input.ReadSingle();
                result.CriticalHitScalar = input.ReadSingle();
                result.Radius = input.ReadInt32();

                return result;
            }
        }

        #endregion
    }
}
