﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Engine
{
    public class VideoManager
    {
        #region Properties and Fields

        public VideoPlayer Player
        {
            get;
            private set;
        }

        public Video ActiveVideo
        {
            get;
            private set;
        }

        public string ContentFolder
        {
            get;
            private set;
        }

        public Vector2 Position
        {
            get;
            private set;
        }

        public bool IsDonePlaying
        {
            get { return Player.State == MediaState.Stopped; }
        }

        private ContentManager Content
        {
            get;
            set;
        }

        public static VideoManager singleton;

        #endregion

        #region Constructor

        public VideoManager(ContentManager content, string dir)
        {
            Content = content;
            ContentFolder = dir;
            singleton = this;

            Player = new VideoPlayer();
            Position = Vector2.Zero;
        }

        #endregion

        #region Methods

        public void LoadVideo(string videoAsset, Vector2 pos)
        {
            if (Player.State == MediaState.Playing) 
                Player.Stop();

            ActiveVideo = Content.Load <Video> (ContentFolder + videoAsset);
            Position = pos;
        }

        public void Play()
        {
            if (ActiveVideo != null)
                Player.Play(ActiveVideo);
        }

        public void Stop()
        {
            Player.Stop();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(
                Player.GetTexture(), 
                new Rectangle((int)Position.X, (int)Position.Y, 1280, 1024), 
                Color.White);
        }

        #endregion
    }
}
