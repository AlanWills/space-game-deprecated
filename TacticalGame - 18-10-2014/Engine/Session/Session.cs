﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Engine
{
    public class TurretEventArgs : EventArgs
    {
        public Turret t;
        public TurretEventArgs(Turret t)
        {
            this.t = t;
        }
    }

    public class ShipEventArgs : EventArgs
    {
        public Ship s;
        public ShipEventArgs(Ship s)
        {
            this.s = s;
        }
    }

    public class Session
    {
        public delegate void PurchaseTurretEventHandler(object sender, TurretEventArgs ptea);
        public delegate void SellTurretEventHandler(object sender, TurretEventArgs stea);
        public delegate void PurchaseShipEventHandler(object sender, ShipEventArgs psea);
        public delegate void SellShipEventHandler(object sender, ShipEventArgs ssea);

        #region Properties and Fields

        public Map Map
        {
            get;
            private set;
        }

        public string MoneyDisplay
        {
            get;
            private set;
        }

        public Player ActivePlayer
        {
            get;
            private set;
        }

        public Ally SelectedShip
        {
            get;
            private set;
        }

        public UserInterface UI
        {
            get;
            private set;
        }

        public bool IsPaused
        {
            get;
            private set;
        }

        public float MinSpeed
        {
            get;
            private set;
        }

        public float Speed
        {
            get;
            private set;
        }

        public float MaxSpeed
        {
            get;
            private set;
        }

        public static Session singleton;

        #endregion

        #region Events

        public event PurchaseTurretEventHandler TurretPurchased;
        public event SellTurretEventHandler TurretSold;
        public event PurchaseShipEventHandler ShipPurchased;
        public event SellShipEventHandler ShipSold;
        public event EventHandler MoneyIncreased;
        public event EventHandler MapFinished;

        #endregion

        #region Constructor

        public Session(Map map)
        {
            Map = map;
            ActivePlayer = new Player();
            ActivePlayer.Money = (uint)map.Money;

            MoneyDisplay = String.Format("Available Money: {0}", ActivePlayer.Money);

            singleton = this;
            IsPaused = false;

            MinSpeed = 0.5f;
            Speed = 1.0f;
            MaxSpeed = 2.0f;
        }

        #endregion

        #region Methods

        public void SetUI(UserInterface UI)
        {
            this.UI = UI;
        }

        public void Update(GameTime gameTime)
        {
            if (!IsPaused)
            {
                UI.UpdateUI(gameTime);

                foreach (Ally a in ActivePlayer.PlayerShips)
                {
                    a.Update(gameTime);
                }

                Map.Update(gameTime);

                if (Map.State == MapState.Finished && MapFinished != null)
                {
                    MapFinished(this, EventArgs.Empty);
                }
            }
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            foreach (Ally s in ActivePlayer.PlayerShips)
            {
                s.Draw(gameTime, spriteBatch);
            }

            foreach (Enemy e in Map.ActiveWave.Enemies)
            {
                e.Draw(gameTime, spriteBatch);
            }
        }

        public void SelectShip(Ally s)
        {
            SelectedShip = s;
        }

        public void DeselectShip()
        {
            SelectedShip = null;
        }

        internal void PurchaseTurret(Turret t, Point mapLocation)
        {
            if (t.Cost <= ActivePlayer.Money)
            {
                if (!t.Initialized) t.Initialize(SelectedShip);
                ActivePlayer.Money -= (uint)t.Cost;
                MoneyDisplay = String.Format("Available Money: {0}", ActivePlayer.Money);

                // t.MapLocation = mapLocation;
                SelectedShip.Turrets.Add(t);

                if (TurretPurchased != null)
                {
                    TurretPurchased(this, new TurretEventArgs(t));
                }
            }
        }

        internal void UpgradeTurret(Turret t)
        {
            if (t.UpgradeCost <= ActivePlayer.Money && t.Level + 1 < t.MaxLevel)
            {
                ActivePlayer.Money -= (uint)t.UpgradeCost;
                MoneyDisplay = String.Format("Available Money: {0}", ActivePlayer.Money);
                t.Upgrade();
            }
        }

        internal void SellTurret(Turret t)
        {
            ActivePlayer.Money += (uint)(t.TotalCost * t.SellScalar);
            MoneyDisplay = String.Format("Available Money: {0}", ActivePlayer.Money);
            SelectedShip.Turrets.Remove(t);

            if (TurretSold != null)
            {
                TurretSold(this, new TurretEventArgs(t));
            }
        }

        internal void AddMoney(int p)
        {
            ActivePlayer.Money += (uint)p;
            MoneyDisplay = String.Format("Available Money: {0}", ActivePlayer.Money);

            if (MoneyIncreased != null)
            {
                MoneyIncreased(this, EventArgs.Empty);
            }
        }

        internal void IncreaseSpeed(float amount)
        {
            if (Speed < MaxSpeed)
            {
                Speed += amount;
                MathHelper.Clamp(Speed, MinSpeed, MaxSpeed);
            }
        }

        internal void DecreaseSpeed(float amount)
        {
            if (Speed > MinSpeed)
            {
                Speed -= amount;
                MathHelper.Clamp(Speed, MinSpeed, MaxSpeed);
            }
        }

        public void Pause()
        {
            IsPaused = true;
        }

        public void Resume()
        {
            IsPaused = false;
        }

        #endregion
    }
}
