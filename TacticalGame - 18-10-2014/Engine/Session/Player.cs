﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Engine
{
    public class Player
    {
        #region Properties and Fields

        public uint Money
        {
            get;
            set;
        }

        public List<Ally> PlayerShips
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public Player()
        {
            PlayerShips = new List<Ally>();
        }

        #endregion
    }
}
