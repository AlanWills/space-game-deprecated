﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using Microsoft.Xna.Framework;

namespace Engine
{
    public class Wave
    {
        #region Writer

        [ContentTypeWriter]
        public class WaveWriter : ContentTypeWriter<Wave>
        {
            protected override void Write(ContentWriter output, Wave value)
            {
                output.WriteObject(value.EnemyAssets);
                output.Write(value.Title);
                output.Write(value.EnemySpawnTimer);
                output.Write(value.MoneyPerKill);
            }

            public override string GetRuntimeReader(Microsoft.Xna.Framework.Content.Pipeline.TargetPlatform targetPlatform)
            {
                return typeof(Wave.WaveReader).AssemblyQualifiedName;
            }
        }

        #endregion

        #region Properties

        [ContentSerializer]
        private List<string> EnemyAssets
        {
            get;
            set;
        }

        [ContentSerializerIgnore]
        public List<Enemy> Enemies
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        private List<Enemy> AllEnemies
        {
            get;
            set;
        }

        [ContentSerializerIgnore]
        public List<Enemy> EnemiesToRemove
        {
            get;
            protected set;
        }

        [ContentSerializerIgnore]
        public Map Map
        {
            get;
            private set;
        }

        [ContentSerializer]
        public string Title
        {
            get;
            private set;
        }

        [ContentSerializer]
        public float EnemySpawnTimer
        {
            get;
            set;
        }

        [ContentSerializer]
        public int MoneyPerKill
        {
            get;
            set;
        }

        [ContentSerializerIgnore]
        public bool IsDone
        {
            get;
            protected set;
        }

        #endregion

        #region Constructor

        public Wave() 
        {
            EnemyAssets = new List<string>();
            Enemies = new List<Enemy>();
            AllEnemies = new List<Enemy>();
            EnemiesToRemove = new List<Enemy>();
        }

        #endregion

        #region Initialize

        public void Initialize(Map map) 
        {
            Map = map;
            Reset();
        }

        #endregion

        #region Update

        public void Update(GameTime gameTime) 
        {
            if (!IsDone)
            {
                if (Enemies.Count == 0)
                {
                    IsDone = true;
                    return;
                }

                foreach (Enemy m in EnemiesToRemove)
                {
                    Enemies.Remove(m);
                }

                EnemiesToRemove.Clear();

                foreach (Enemy m in Enemies)
                {
                    m.Update(gameTime);
                }
            }
        }

        #endregion

        #region Methods

        public void Remove(Ship s) 
        {
            Enemy e = s as Enemy;
            EnemiesToRemove.Add(e);
            s.Turrets.Clear();
        }

        public void Reset() 
        {
            if (Enemies.Count != AllEnemies.Count)
            {
                Enemies.Clear();
                foreach (Enemy e in AllEnemies)
                {
                    Enemies.Add(e.Clone());
                }
            }

            for (int i = 0; i < Enemies.Count; i++)
            {
                Enemies[i].Wave = this;
                Enemies[i].Delay = EnemySpawnTimer * (i + 1);
                Enemies[i].Position = new Vector2(0, 0);
            }

            IsDone = false;
        }

        public override string ToString()
        {
            return Title;
        }

        #endregion

        #region Reader

        public class WaveReader : ContentTypeReader<Wave>
        {
            protected override Wave Read(ContentReader input, Wave existingInstance)
            {
                Wave wave = new Wave();

                wave.EnemyAssets.AddRange(input.ReadObject<List<string>>());
                Enemy e = null;

                foreach (string s in wave.EnemyAssets)
                {
                    e = input.ContentManager.Load<Enemy>(String.Format("Enemies\\{0}", s));
                    wave.Enemies.Add(e.Clone());
                    wave.AllEnemies.Add(e.Clone());
                }

                wave.Title = input.ReadString();
                wave.EnemySpawnTimer = input.ReadSingle();
                wave.MoneyPerKill = input.ReadInt32();

                return wave;
            }
        }

        #endregion

        internal Wave Clone()
        {
            Wave w = new Wave();

            foreach (Enemy e in Enemies)
            {
                w.Enemies.Add(e.Clone());
                w.AllEnemies.Add(e.Clone());
            }

            w.Title = Title.Clone().ToString();
            w.EnemySpawnTimer = EnemySpawnTimer;
            w.MoneyPerKill = MoneyPerKill;

            return w;
        }
    }
}
