﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.IO;
using System.Xml.Serialization;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;

namespace Engine
{
    public enum MapState
    {
        WaveDelay,
        Active,
        Finished
    }

    public class Map
    {
        #region Writer

        [ContentTypeWriter]
        public class MapWriter : ContentTypeWriter<Map>
        {
            protected override void Write(ContentWriter output, Map value)
            {
                output.Write(value.Name);
                output.WriteObject(value.Dimensions);
                output.Write(value.Description);
                output.Write(value.ThumbnailAsset);
                output.Write(value.Endless);
                output.WriteObject(value.WavesAvailable);
                output.Write(value.WaveDelay);
                output.Write(value.Difficulty);
                output.Write(value.Money);
                output.WriteObject(value.ForeColourArray);
                output.WriteObject(value.ErrorColourArray);
                output.Write(value.InfoBarBackgroundAsset);
                output.WriteObject(value.BorderColourArray);
                output.Write(value.BorderTextureAsset);
                output.Write(value.MouseTextureAsset);
                output.Write(value.SmallErrorButtonTextureAsset);
                output.Write(value.SmallNormalButtonTextureAsset);
                output.Write(value.LargeErrorButtonTextureAsset);
                output.Write(value.LargeNormalButtonTextureAsset);
                output.Write(value.BackgroundTextureAsset);
                output.Write(value.SongCueName);

            }

            public override string GetRuntimeReader(Microsoft.Xna.Framework.Content.Pipeline.TargetPlatform targetPlatform)
            {
                return typeof(Map.MapReader).AssemblyQualifiedName;
            }
        }

        #endregion

        #region Properties and Fields

        [ContentSerializerIgnore]
        public MapState State
        {
            get;
            private set;
        }

        [ContentSerializer]
        public string Name
        {
            get;
            private set;
        }

        [ContentSerializer]
        public Point Dimensions
        {
            get;
            private set;
        }
        
        [ContentSerializer]
        public string Description
        {
            get;
            private set;
        }

        [ContentSerializer(Optional = true)]
        private string ThumbnailAsset
        {
            get;
            set;
        }

        [ContentSerializerIgnore]
        public Texture2D Thumbnail
        {
            get;
            private set;
        }

        [ContentSerializer]
        public bool Endless
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public string TurretsInfo
        {
            get;
            private set;
        }

        [ContentSerializer]
        public List<string> WavesAvailable
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public List<Wave> WaveList
        {
            get;
            private set;
        }

        [ContentSerializer]
        private float WaveDelay
        {
            get;
            set;
        }

        [ContentSerializerIgnore]
        public float Timer
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public int WaveIndex
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public Wave ActiveWave
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public string WavesInfo
        {
            get;
            private set;
        }

        [ContentSerializer]
        public int Difficulty
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public string DifficultyInfo
        {
            get;
            private set;
        }

        int money;
        [ContentSerializer]
        public int Money
        {
            get { return money; }
            set
            {
                money = value;
                MoneyInfo = String.Format("Money: {0}", money);
            }
        }

        [ContentSerializerIgnore]
        public string MoneyInfo
        {
            get;
            private set;
        }

        [ContentSerializer]
        private int[] ForeColourArray
        {
            get;
            set;
        }

        [ContentSerializerIgnore]
        public Color ForeColour
        {
            get;
            private set;
        }

        [ContentSerializer]
        private int[] ErrorColourArray
        {
            get;
            set;
        }

        [ContentSerializerIgnore]
        public Color ErrorColour
        {
            get;
            private set;
        }

        [ContentSerializer]
        private string InfoBarBackgroundAsset
        {
            get;
            set;
        }

        [ContentSerializerIgnore]
        public Texture2D InfoBarBackground
        {
            get;
            private set;
        }

        [ContentSerializer]
        private int[] BorderColourArray
        {
            get;
            set;
        }

        [ContentSerializerIgnore]
        public Color BorderColour
        {
            get;
            private set;
        }

        [ContentSerializer]
        private string BorderTextureAsset
        {
            get;
            set;
        }

        [ContentSerializerIgnore]
        public Texture2D BorderTexture
        {
            get;
            private set;
        }

        [ContentSerializer]
        private string MouseTextureAsset
        {
            get;
            set;
        }

        [ContentSerializerIgnore]
        public Texture2D MouseTexture
        {
            get;
            private set;
        }

        [ContentSerializer]
        private string SmallErrorButtonTextureAsset
        {
            get;
            set;
        }

        [ContentSerializerIgnore]
        public Texture2D SmallErrorButtonTexture
        {
            get;
            private set;
        }

        [ContentSerializer]
        private string SmallNormalButtonTextureAsset
        {
            get;
            set;
        }

        [ContentSerializerIgnore]
        public Texture2D SmallNormalButtonTexture
        {
            get;
            private set;
        }

        [ContentSerializer]
        private string LargeErrorButtonTextureAsset
        {
            get;
            set;
        }

        [ContentSerializerIgnore]
        public Texture2D LargeErrorButtonTexture
        {
            get;
            private set;
        }

        [ContentSerializer]
        private string LargeNormalButtonTextureAsset
        {
            get;
            set;
        }

        [ContentSerializerIgnore]
        public Texture2D LargeNormalButtonTexture
        {
            get;
            private set;
        }

        [ContentSerializer]
        private string BackgroundTextureAsset
        {
            get;
            set;
        }

        [ContentSerializerIgnore]
        public Texture2D BackgroundTexture
        {
            get;
            private set;
        }

        [ContentSerializer]
        public string SongCueName
        {
            get;
            private set;
        }

        public event EventHandler NewWave;

        #endregion

        #region Constructor

        public Map() 
        {
            WavesAvailable = new List<string>();
            TurretsInfo = string.Empty;
            WavesInfo = string.Empty;
            DifficultyInfo = string.Empty;
            WaveIndex = 0;
        }

        #endregion

        #region Methods

        public void Update(GameTime gameTime)
        {
            if (ActiveWave.IsDone)
            {
                /*if (WaveIndex >= WaveList.Count - 1)
                {
                    if (Endless) WaveIndex = 0;
                    else { State = MapState.Finished; }
                }

                if (State != MapState.Finished)
                {
                    if (WaveDelay > 0)
                    {
                        Timer = WaveDelay;
                        State = MapState.WaveDelay;
                    }
                    WaveIndex++;
                    ActiveWave = WaveList[WaveIndex];
                    ActiveWave.Reset();

                    if (NewWave != null)
                        NewWave(this, EventArgs.Empty);
                }*/
            }
            else
            {
                ActiveWave.Update(gameTime);
            }
        }

        public void StartNextWaveNow()
        {
            Timer = 0;
            State = MapState.Active;
        }

        public void Reset()
        {
            WaveIndex = 0;

            foreach (Wave w in WaveList)
                w.Reset();

            ActiveWave = WaveList[WaveIndex];
            State = MapState.WaveDelay;
        }

        #endregion

        #region Content Reader

        public class MapReader : ContentTypeReader<Map>
        {
            protected override Map Read(ContentReader input, Map existingInstance)
            {
                Map map = new Map();

                map.Name = input.ReadString();
                map.Dimensions = input.ReadObject<Point>();
                map.Description = input.ReadString();
                map.ThumbnailAsset = input.ReadString();
                if (string.IsNullOrEmpty(map.ThumbnailAsset))
                {
                    map.ThumbnailAsset = "nothumb";
                    map.Thumbnail = input.ContentManager.Load<Texture2D>(String.Format("Textures\\Maps\\{0}", map.ThumbnailAsset));
                }
                else
                {
                    map.Thumbnail = input.ContentManager.Load<Texture2D>(String.Format("Textures\\Maps\\{0}\\{1}", map.Name, map.ThumbnailAsset));
                }

                map.Endless = input.ReadBoolean();

                map.WavesAvailable.AddRange(input.ReadObject<List<string>>());
                map.WavesInfo = String.Format("Waves: {0}", map.WavesAvailable.Count);

                map.WaveList = new List<Wave>(map.WavesAvailable.Count);
                foreach (string s in map.WavesAvailable)
                {
                    Wave w = input.ContentManager.Load<Wave>(String.Format("Wave\\{0}", s)).Clone();
                    w.Initialize(map);
                    map.WaveList.Add(w);
                }

                map.ActiveWave = map.WaveList[0];
                map.WaveIndex = 0;

                map.WaveDelay = input.ReadSingle();

                map.Difficulty = input.ReadInt32();
                map.DifficultyInfo = String.Format("Difficulty: {0}", map.Difficulty);

                map.Money = input.ReadInt32();
                map.ForeColourArray = input.ReadObject<int[]>();
                if (map.ForeColourArray.Length == 4)
                {
                    map.ForeColour = new Color(map.ForeColourArray[0], map.ForeColourArray[1], map.ForeColourArray[2], map.ForeColourArray[3]);
                }
                else
                {
                    throw new Exception(input.AssetName + ".xml must have 4 integers to represent RGBA color data in the ForeColourArray tag.");
                }

                map.ErrorColourArray = input.ReadObject<int[]>();
                if (map.ErrorColourArray.Length == 4)
                {
                    map.ErrorColour = new Color(map.ErrorColourArray[0], map.ErrorColourArray[1], map.ErrorColourArray[2], map.ErrorColourArray[3]);
                }
                else
                {
                    throw new Exception(input.AssetName + ".xml must have 4 integers to represent RGBA color data in the ErrorColourArray tag.");
                }

                map.InfoBarBackgroundAsset = input.ReadString();
                if (!string.IsNullOrEmpty(map.InfoBarBackgroundAsset)) map.InfoBarBackground = input.ContentManager.Load<Texture2D>(String.Format("Textures\\Maps\\{0}\\{1}", map.Name, map.InfoBarBackgroundAsset));

                map.BorderColourArray = input.ReadObject<int[]>();
                if (map.BorderColourArray.Length == 4)
                {
                    map.BorderColour = new Color(map.BorderColourArray[0], map.BorderColourArray[1], map.BorderColourArray[2], map.BorderColourArray[3]);
                }

                map.BorderTextureAsset = input.ReadString();
                if (!string.IsNullOrEmpty(map.BorderTextureAsset)) map.BorderTexture = input.ContentManager.Load<Texture2D>(String.Format("Textures\\Maps\\{0}\\{1}", map.Name, map.BorderTextureAsset));

                map.MouseTextureAsset = input.ReadString();
                if (!string.IsNullOrEmpty(map.MouseTextureAsset)) map.MouseTexture = input.ContentManager.Load<Texture2D>(String.Format("Textures\\Maps\\{0}\\{1}", map.Name, map.MouseTextureAsset));

                map.SmallErrorButtonTextureAsset = input.ReadString();
                if (!string.IsNullOrEmpty(map.SmallErrorButtonTextureAsset)) map.SmallErrorButtonTexture = input.ContentManager.Load<Texture2D>(String.Format("Textures\\Maps\\{0}\\{1}", map.Name, map.SmallErrorButtonTextureAsset));

                map.SmallNormalButtonTextureAsset = input.ReadString();
                if (!string.IsNullOrEmpty(map.SmallNormalButtonTextureAsset)) map.SmallNormalButtonTexture = input.ContentManager.Load<Texture2D>(String.Format("Textures\\Maps\\{0}\\{1}", map.Name, map.SmallNormalButtonTextureAsset));

                map.LargeErrorButtonTextureAsset = input.ReadString();
                if (!string.IsNullOrEmpty(map.LargeErrorButtonTextureAsset)) map.LargeErrorButtonTexture = input.ContentManager.Load<Texture2D>(String.Format("Textures\\Maps\\{0}\\{1}", map.Name, map.LargeErrorButtonTextureAsset));

                map.LargeNormalButtonTextureAsset = input.ReadString();
                if (!string.IsNullOrEmpty(map.LargeNormalButtonTextureAsset)) map.LargeNormalButtonTexture = input.ContentManager.Load<Texture2D>(String.Format("Textures\\Maps\\{0}\\{1}", map.Name, map.LargeNormalButtonTextureAsset));

                if (map.SmallNormalButtonTexture.Bounds != map.SmallErrorButtonTexture.Bounds) throw new Exception("The SmallNormalButtonTexture and the SmallErrorButtonTexture must be the same size");
                if (map.LargeNormalButtonTexture.Bounds != map.LargeErrorButtonTexture.Bounds) throw new Exception("The LargeNormalButtonTexture and the LargeErrorButtonTexture must be the same size");

                if (map.WaveDelay > 0)
                {
                    map.State = MapState.WaveDelay;
                    map.Timer = map.WaveDelay;
                }

                map.BackgroundTextureAsset = input.ReadString();
                if (!string.IsNullOrEmpty(map.BackgroundTextureAsset)) map.BackgroundTexture = input.ContentManager.Load<Texture2D>(String.Format("Textures\\Maps\\{0}\\{1}", map.Name, map.BackgroundTextureAsset));

                map.SongCueName = input.ReadString();

                return map;
            }

            private float GetAngleBetweenTwoMapLocations(Point source, Point target)
            {
                if (target.X == -1) return 0.0f;
                else
                {
                    return (float)Math.Atan2(target.Y - source.Y, target.X - source.X);
                }
            }
        }

        #endregion
    }
}
